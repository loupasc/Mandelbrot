package net.fraktales.tool;

/**
 * Image processing tools including Anti-Aliasing.
 * The filter is based on lanczos2 function defined by:
 * lanczos2(x) = 2 * sin(PI * x) * sin (PI * x / 2.0) / sqr(PI * x) (|x| < 2)
 * Filter f(x,y) = lanczos2(x) * lanczos2(y)
 * 
 * @author POE
 * @version 1.1.0
 * @since Jun 11, 2018
 */
public final class ImageUtils {

	private ImageUtils() {
	}

	public static int getAntiAliasingEdgeSize(int inputSize) {
		return 2 * inputSize + 4;
	}

	public static float[] create6x6PixelBloc() {
		return new float[36];
	}

	public static void fillInscribedCircleIn6x6PixelBloc(int width, int offset, float[] bitmap, float[] pixelBloc) {
		offset = offset - 2 * width - 2;
		System.arraycopy(bitmap, offset, pixelBloc, 0, 6);
		offset = offset + width;
		System.arraycopy(bitmap, offset, pixelBloc, 6, 6);
		offset = offset + width;
		System.arraycopy(bitmap, offset, pixelBloc, 12, 6);
		offset = offset + width;
		System.arraycopy(bitmap, offset, pixelBloc, 18, 6);
		offset = offset + width;
		System.arraycopy(bitmap, offset, pixelBloc, 24, 6);
		offset = offset + width;
		System.arraycopy(bitmap, offset, pixelBloc, 30, 6);
	}

	public static float convolve(float[] pixelBloc) {
		return (47 * (pixelBloc[14] + pixelBloc[15] + pixelBloc[20] + pixelBloc[21])
			+ 13 * (pixelBloc[8] + pixelBloc[9] + pixelBloc[13] + pixelBloc[16]
				+ pixelBloc[19] + pixelBloc[22] + pixelBloc[26] + pixelBloc[27])
			+ 3 * (pixelBloc[7] + pixelBloc[10] + pixelBloc[25] + pixelBloc[28])
			- 5 * (pixelBloc[2] + pixelBloc[3] + pixelBloc[12] + pixelBloc[17]
				+ pixelBloc[18] + pixelBloc[23] + pixelBloc[32] + pixelBloc[33])
			- 1 * (pixelBloc[1] + pixelBloc[4] + pixelBloc[6] + pixelBloc[11]
				+ pixelBloc[24] + pixelBloc[29] + pixelBloc[31] + pixelBloc[34])
			) / 256;
	}

	// "Optimized" convolution matrix of Lanzcos2/Downscale 2x
	//
	// lanczos2(x) = 2 * sin(PI * x) * sin (PI * x / 2.0) / sqr(PI * x)
	// Float-to-Int : Math.round(61 * lanczos2(x) * lanczos2(y))
	// Matrix[8x8] cropped into Matrix[6x6]
	// => result normalized by 8-bit right shift.
	//
	//       [ 0 -1 -5 -5 -1  0]
	//       [-1  3 13 13  3 -1]
	//       [-5 13 47 47 13 -5]
	//       [-5 13 47 47 13 -5]
	//       [-1  3 13 13  3 -1]
	// 1/256 [ 0 -1 -5 -5 -1  0]
	public static int convolve(int[] pixelBloc, int mask) {
		int shift = Integer.numberOfTrailingZeros(mask);

		return (47 * (((pixelBloc[14] & mask) >> shift) + ((pixelBloc[15] & mask) >> shift)
				+ ((pixelBloc[20] & mask) >> shift) + ((pixelBloc[21] & mask) >> shift))
			+ 13 * (((pixelBloc[8] & mask) >> shift) + ((pixelBloc[9] & mask) >> shift)
				+ ((pixelBloc[13] & mask) >> shift) + ((pixelBloc[16] & mask) >> shift)
				+ ((pixelBloc[19] & mask) >> shift) + ((pixelBloc[22] & mask) >> shift)
				+ ((pixelBloc[26] & mask) >> shift) + ((pixelBloc[27] & mask) >> shift))
			+ 3 * (((pixelBloc[7] & mask) >> shift) + ((pixelBloc[10] & mask) >> shift)
				+ ((pixelBloc[25] & mask) >> shift) + ((pixelBloc[28] & mask) >> shift))
			- 5 * (((pixelBloc[2] & mask) >> shift) + ((pixelBloc[3] & mask) >> shift)
				+ ((pixelBloc[12] & mask) >> shift) + ((pixelBloc[17] & mask) >> shift)
				+ ((pixelBloc[18] & mask) >> shift) + ((pixelBloc[23] & mask) >> shift)
				+ ((pixelBloc[32] & mask) >> shift) + ((pixelBloc[33] & mask) >> shift))
			- 1 * (((pixelBloc[1] & mask) >> shift) + ((pixelBloc[4] & mask) >> shift)
				+ ((pixelBloc[6] & mask) >> shift) + ((pixelBloc[11] & mask) >> shift)
				+ ((pixelBloc[24] & mask) >> shift) + ((pixelBloc[29] & mask) >> shift)
				+ ((pixelBloc[31] & mask) >> shift) + ((pixelBloc[34] & mask) >> shift))
			) >> 8;
	}
}
