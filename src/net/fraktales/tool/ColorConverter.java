package net.fraktales.tool;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import javax.imageio.ImageIO;

public final class ColorConverter {
	private static final String PALETTE_FILENAME = "palette.txt";
	private static final int DEPTH_COEF = 2;
	private static final int[] PALETTE = createPalette();

	private ColorConverter() {
	}

	private static void fillPalette(int i0, int i1, int rgb0, int rgb1, int[] table) {
		int range = i1 - i0;

		// One extra bit for dithering
		int r0 = (rgb0 >> 16) * DEPTH_COEF;
		int g0 = ((rgb0 >> 8) & 0xFF) * DEPTH_COEF;
		int b0 = (rgb0 & 0xFF) * DEPTH_COEF;

		// One extra bit for dithering
		int r1 = (rgb1 >> 16) * DEPTH_COEF;
		int g1 = ((rgb1 >> 8) & 0xFF) * DEPTH_COEF;
		int b1 = (rgb1 & 0xFF) * DEPTH_COEF;

		for (int i = 0; i < range; i++) {
			int r = (i * r1 + (range - i) * r0) / range;
			int g = (i * g1 + (range - i) * g0) / range;
			int b = (i * b1 + (range - i) * b0) / range;
			// 32bits integer RGB => store 3x9bits
			table[i + i0] = b | (g << 9) | (r << 18);
		}
	}

//	private static void setRosePalette(int[] table) {
//		fillPalette(  0,  153, 0x000000, 0x2F006F, table);
//		fillPalette(153,  549, 0x2F006F, 0xDF001F, table);
//		fillPalette(549,  829, 0xDF001F, 0xFF4FAF, table);
//		fillPalette(829, 1276, 0xFF4FAF, 0xFFFFFF, table);
//		fillPalette(1276, 7180, 0xFFFFFF, 0xFFFFFF, table);
//		fillPalette(7180, 11276, 0xFFFFFF, 0x000000, table);
//	}

//	private static void setBluePalette(int[] table) {
//		fillPalette(  0,  395, 0x001F00, 0x000033, table);
//		fillPalette(395,  499, 0x000033, 0x000066, table);
//		fillPalette(499,  944, 0x000066, 0x3399CC, table);
//		fillPalette(944, 1276, 0x3399CC, 0xFFFFFF, table);
//	}

//	private static void setDragonPalette(int[] table) {
//		fillPalette(   0,  102, 0x232323, 0x3F0000, table);
//		fillPalette( 102,  370, 0x3F0000, 0xFF3F00, table);
//		fillPalette( 370,  499, 0xFF3F00, 0xFF8F00, table);
//		fillPalette( 499,  880, 0xFF8F00, 0xFFFF00, table);
//		fillPalette( 880,  995, 0xFFFF00, 0xFFFFFF, table);
//		fillPalette( 995, 1107, 0xFFFFFF, 0xC7C7C7, table);
//		fillPalette(1107, 1276, 0xC7C7C7, 0x000000, table);
//	}

	private static void setDefaultPalette(int[] table) {
		fillPalette(0, 255, 0x000000, 0x0000FF, table);
		fillPalette(255, 510, 0x0000FF, 0xFF0000, table);
		fillPalette(510, 765, 0xFF0000, 0xFFFF00, table);
		fillPalette(765, 1020, 0xFFFF00, 0xFFFFFF, table);
		fillPalette(1020, 1275, 0xFFFFFF, 0x000000, table);
		fillPalette(1275, 2550, 0x000000, 0x0000FF, table);
		fillPalette(2550, 3825, 0x0000FF, 0xFF0000, table);
		fillPalette(3825, 5100, 0xFF0000, 0xFFFF00, table);
		fillPalette(5100, 6376, 0xFFFF00, 0xFFFFFF, table);
	}

	//
	// Returns the pair [ index | RGB888 ]
	//
	private static int[] parseLineRead(String lineRead) {
		String lineToProcess = lineRead.trim();
		int indexOfSeparator = lineToProcess.indexOf(',');

		if (indexOfSeparator == -1) {
			throw new IllegalArgumentException("Cannot find color separator");
		}

		if (indexOfSeparator == 0) {
			throw new IllegalArgumentException("Missing color index");
		}

		String[] data = lineToProcess.split(",");
		int[] colorInfo = new int[2];
		colorInfo[0] = Integer.parseInt(data[0].trim());

		if (data.length == 1) {
			colorInfo[1] = 0;
		}
		else {
			colorInfo[1] = Integer.parseInt(data[1].trim(), 16);
		}

		return colorInfo;
	}

	private static int[] createPalette() {
		int[] rgbTable = new int[6376];

		FileReader paletteFileReader = null;
		BufferedReader colorDeclaration = null;

		try {
			paletteFileReader = new FileReader(new File(PALETTE_FILENAME));
			colorDeclaration = new BufferedReader(paletteFileReader);
			String lineRead = colorDeclaration.readLine();
			int[] colorInfo = parseLineRead(lineRead);
			int i0 = colorInfo[0];
			int rgb0 = colorInfo[1];

			lineRead = colorDeclaration.readLine();
			if (lineRead != null) {
				do {
					colorInfo = parseLineRead(lineRead);
					int i1 = colorInfo[0];
					int rgb1 = colorInfo[1];
					fillPalette(i0, i1, rgb0, rgb1, rgbTable);
					i0 = i1;
					rgb0 = rgb1;
					lineRead = colorDeclaration.readLine();
				} while (lineRead != null);
			}
			else {
				throw new IllegalArgumentException("Two color declaration needed at least");
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			setDefaultPalette(rgbTable);
		}
		finally {
			if (paletteFileReader != null) {
				try {
					paletteFileReader.close();
				}
				catch (IOException e) {
					e.printStackTrace();
				}
				try {
					colorDeclaration.close();
				}
				catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return rgbTable;
	}

	//
	// High color depth converted to RGB888
	//
	public static int get24BitColor(int r, int g, int b, int ditherPattern) {
		r = (r + ditherPattern) / DEPTH_COEF;
		g = (g + ditherPattern) / DEPTH_COEF;
		b = (b + ditherPattern) / DEPTH_COEF;

		return b | (g << 8) | (r << 16);
	}

	//
	// Levels to RGB color components packed
	//
	public static int getRGBFromLevel(float level) {
		int truncatedLevel = (int) level;
		float coef1 = level - truncatedLevel;
		float coef0 = 1 - coef1;

		int r0 = PALETTE[truncatedLevel] >> 18;
		int g0 = (PALETTE[truncatedLevel] >> 9) & 0x1FF;
		int b0 = PALETTE[truncatedLevel] & 0x1FF;
		int r1 = PALETTE[truncatedLevel + 1] >> 18;
		int g1 = (PALETTE[truncatedLevel + 1] >> 9) & 0x1FF;
		int b1 = PALETTE[truncatedLevel + 1] & 0x1FF;

		return Math.round(coef0 * b0 + coef1 * b1) | (Math.round(coef0 * g0 + coef1 * g1) << 9) | (Math.round(coef0 * r0 + coef1 * r1) << 18);
	}

	public static void main(String[] args) {
		BufferedImage gradient = new BufferedImage(1276, 256, BufferedImage.TYPE_INT_RGB);
		int[] output = ((DataBufferInt) gradient.getRaster().getDataBuffer()).getData();

		int checksPattern = 0;
		int jMod2 = 0;
		for (int j = 0; j < 256; j++) {
			jMod2 = j & 1;
			for (int i = 0; i < 1276; i++) {
				//     | i j
				// 0 1 | 0 0 0
				// 1 0 | 0 1 1
				//     | 1 0 1
				//     | 1 1 0
				// ==> i xor j
				checksPattern = (i & 1) ^ jMod2;
				int hiColorDepthRGB = getRGBFromLevel(i);
				output[1276 * j + i] = get24BitColor(hiColorDepthRGB >> 18,
													(hiColorDepthRGB >> 9) & 0x1FF,
													hiColorDepthRGB & 0x1FF,
													checksPattern);
			}
		}

		try {
			ImageIO.write(gradient, "bmp", new File("gradient.bmp"));
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
}
