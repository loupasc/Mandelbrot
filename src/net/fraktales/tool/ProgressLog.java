package net.fraktales.tool;

public class ProgressLog {
	private static final char[] UNIT = { '0', '3', '5', '7', '0', '2', '5' , '8'};
	private static final char[] TEN = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
	private final char[] value = { '0', '0', '%' };
	private final int range;
	private final int[] indexToDisplay;
	private int arrayIndex;

	public ProgressLog(int range) {
		this.range = range;
		indexToDisplay = new int[40];
	}

	public void initialize() {
		for (int i = 0; i < 40; i++) {
			indexToDisplay[i] = (i * range) / 40;
		}
		arrayIndex = 0;
	}

	public String getPercentageFrom(int index) {

		if (arrayIndex == 40) return null;

		String percentageUpdated = null;
		if (index == indexToDisplay[arrayIndex]) {
			value[0] = TEN[arrayIndex >> 2];
			value[1] = UNIT[arrayIndex & 0x7];
			percentageUpdated = new String(value);

			arrayIndex = arrayIndex + 1;
		}

		return percentageUpdated;
	}
}
