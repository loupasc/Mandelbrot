// This code is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.

package net.fraktales.math;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;

/**
 * The <b>FixedPointNumber64</b> class provides a set of utility methods allowing to perform basic operations based on fixed-point arithmetic.<br>
 * The fixed-point number is backed by a 64-bit signed integer.<br>
 * Java {@link Double} floating-point has 52-bit precisions.
 * In case of Mandelbrot computation needs it might be insufficient; especially when two values are added and when these values have very different magnitude orders.<br>
 * This phenomenon is known as absorption.<br>
 * Java {@link BigDecimal} offers an arbitrary-precision decimal number but it is too slow to perform fractal computations.<br>
 * The <b>FixedPointNumber64</b> places as a compromise between performance and precision; the fixed-point implementation offers 61-bit significand precision.<br>
 * Bits:
 * <code>[ sign 63 ] [integer part 62 61 ] [ fractional part 60 59 ... 0 ]</code>
 * <br>
 * <code>[ 0x8000000000000000 ]</code> is a constant corresponding to values outside ]-4; 4[ range.
 * <br>
 * Other values are comprises between -4 and 4 which is sufficient to compute Mandelbrot fractals.
 * 
 * @author Pascal Ollive
 * @version 1.0.11
 * @since Jun 11, 2018
 */
public final class FixedPointNumber64 {

	/**
	 * Fractional part number of bits.
	 */
	private static final int FRACTIONAL_WIDTH = 61;

	/**
	 * Constant corresponding to an out of range value.
	 */
	private static final long OUT_OF_RANGE = Long.MIN_VALUE;

	/**
	 * Long.MAX_VALUE wrapped in a BigDecimal number.
	 */
	private static final BigDecimal LONG_MAX_VALUE = new BigDecimal(Long.MAX_VALUE);

	/**
	 * Offset 1 << 61 used by arbitrary precision operations.
	 */
	private static final BigDecimal OFFSET = new BigDecimal(1L << FRACTIONAL_WIDTH);

	/**
	 * Number of digits which can be represented in base 10 see {@link #toDecimal()}.
	 */
	private static final int NB_DECIMAL_DIGITS = 19;

	/**
	 * Maximum precision in base 10 see {@link #toDecimal()}.
	 */
	private static final BigDecimal EPSILON = BigDecimal.valueOf(1, NB_DECIMAL_DIGITS);

	/**
	 * Private constructor prevents callers from constructing objects of this class.
	 */
	private FixedPointNumber64() {
	}

	/**
	 * Returns a fixed-point number backed by a 64-bit signed integer.
	 * The number is converted from the specified double value (IEEE 754 floating-point double format).
	 * The number represents a value comprise between -4 and 4 (-4 and 4 excluded).<br>
	 * <br>
	 * in: s|exp - 1023|mantissa<br>
	 * out:<br>
	 * ]-4; 4[<br>
	 * Bits:
	 * <code>[sign 63] [integer part 62 61 ] [ fractional part 60 59 ... 0 ]</code>
	 * <br>
	 * Validity of the result can be checked with {@link #isOutOfRange(long)} method.
	 * @param d floating-point to convert
	 * @return fixed-point number
	 */
	public static long getFromDoublePrecision(double d) {
		if (Math.getExponent(d) < 2) {
			return Math.round(Math.scalb(d, FRACTIONAL_WIDTH));
		}

		return OUT_OF_RANGE;
	}

	/**
	 * Returns the binary representation of the specified 64-bit fixed point number.
	 * 
	 * @param n fixed-point number
	 * @return a string containing the binary representation
	 */
	public static String toBinaryString(long n) {
		if (n == OUT_OF_RANGE) {
			return Double.toString(Double.NaN);
		}
		int sign = Long.signum(n);
		long absN = sign * n;
		String s = Long.toBinaryString(absN);
		StringBuilder sb = (sign == -1 ? new StringBuilder("-") : new StringBuilder("+"));
		int nbLeadingZeros = 63 - s.length();
		for (int i = 0; i < nbLeadingZeros; i++) {
			sb.append(0);
		}
		sb.append(s);
		sb.insert(3, '.');

		return sb.toString();
	}

	/**
	 * Returns a BigDecimal containing the value of the specified 64-bit fixed point number.
	 * 
	 * @param n fixed-point number
	 * @return a BigDecimal
	 */
	public static BigDecimal toDecimal(long n) {
		if (n == OUT_OF_RANGE) {
			return BigDecimal.valueOf(4);
		}

		BigDecimal decimal = BigDecimal.valueOf(n).divide(OFFSET, new MathContext(NB_DECIMAL_DIGITS)).stripTrailingZeros();

		BigDecimal closestValue = decimal.subtract(EPSILON).stripTrailingZeros();
		if (closestValue.scale() < decimal.scale()) {
			return closestValue;
		}
		else if (closestValue.scale() == decimal.scale()) {
			// Decimal representation might be shortened
			closestValue = decimal.add(EPSILON).stripTrailingZeros();
			if (closestValue.scale() < decimal.scale()) {
				return closestValue;
			}
		}

		return decimal;
	}

	/**
	 * Returns the decimal representation of the specified 64-bit fixed point number.
	 * 
	 * @param n fixed-point number
	 * @return a string containing the decimal representation
	 */
	public static String toString(long n) {
		if (n == OUT_OF_RANGE) {
			return Double.toString(Double.NaN);
		}

		BigDecimal decimal = toDecimal(n);
		return decimal.toPlainString();
	}

	/**
	 * Returns the integer part of the specified 64-bit fixed point number.
	 * 
	 * @param n fixed-point number
	 * @return an integer
	 */
	public static int toInteger(long n) {
		if (n == OUT_OF_RANGE) {
			return 4;
		}

		int sign = Long.signum(n);
		long absN = sign * n;

		return (int) (absN >> FRACTIONAL_WIDTH) * sign;
	}

	/**
	 * Returns the floating-point value corresponding to the fixed-point number.
	 * 
	 * @param n fixed-point number to convert
	 * @return a floating-point value
	 */
	public static double toDoublePrecision(long n) {
		if (n == OUT_OF_RANGE) {
			return Double.NaN;
		}

		//
		// Implicit cast of n is float
		// => explicit cast double to keep precision
		//
		return Math.scalb((double) n, -FRACTIONAL_WIDTH);
	}

	/**
	 * Returns a fixed-point number backed by a 64-bit signed integer from the specified decimal number.
	 * Output:
	 * <code>[sign 63] [integer part 62 61 ] [ fractional part 60 59 ... 0 ]</code>
	 * 
	 * @param d decimal number to convert
	 * @return fixed-point number
	 */
	public static long getFromBigDecimal(BigDecimal d) {
		BigDecimal scaledValue = OFFSET.multiply(d);
		int sign = scaledValue.signum();

		if (sign == -1) {
			scaledValue = scaledValue.negate();
		}

		if (scaledValue.compareTo(LONG_MAX_VALUE) > 0) {
			return OUT_OF_RANGE;
		}

		return sign * scaledValue.longValue();
	}

	/**
	 * Returns a fixed-point number backed by a 64-bit signed integer from the specified string representation.
	 * 
	 * @param s string representation of the number
	 * @return fixed-point number
	 */
	public static long getFromDecimal(String s) {
		return getFromBigDecimal(new BigDecimal(s));
	}

	/**
	 * Returns a fixed-point number backed by a 64-bit signed integer from the specified string representation.
	 * The returned value is never out of range and forced to be between -4 and 4.
	 * 
	 * @param s string representation of the number
	 * @return fixed-point number
	 */
	public static long getBoundedValueFromDecimal(String s) {
		BigDecimal d = new BigDecimal(s);
		int sign = d.signum();
		long n = getFromBigDecimal(d);

		if (n == OUT_OF_RANGE) {
			return sign * Long.MAX_VALUE;
		}

		return n;
	}

	private static long rangeCheckedSum(long n1, long n2) {
		if (n1 > Long.MAX_VALUE - n2) {
			return OUT_OF_RANGE;
		}
		return n1 + n2;
	}

	/**
	 * Returns a fixed-point number backed by a 64-bit signed integer whose value is {@code (n1 + n2)}.
	 * 
	 * @param n1 fixed-point number
	 * @param n2 fixed-point number
	 * @return fixed-point number equals to n1 + n2
	 */
	public static long sum(long n1, long n2) {
		if ((n1 == OUT_OF_RANGE) || (n2 == OUT_OF_RANGE)) {
			return OUT_OF_RANGE;
		}

		if (Long.signum(n1) != Long.signum(n2)) {
			return n1 + n2;
		}

		if (n2 < 0) {
			long absN1 = -n1;
			long absN2 = -n2;
			long absSum = rangeCheckedSum(absN1, absN2);
			if (absSum == OUT_OF_RANGE) {
				return OUT_OF_RANGE;
			}
			return -absSum;
		}

		return rangeCheckedSum(n1, n2);
	}

	/**
	 * Returns a fixed-point number backed by a 64-bit signed integer whose value is {@code (n + d)}.
	 * This method provides a greater range than {@link #sum(long, long)} of results especially when abs(d) > 4.
	 * 
	 * @param n fixed-point number
	 * @param d floating-point
	 * @return fixed-point number equals to n + d
	 */
	public static long sum(long n, double d) {
		if (n == OUT_OF_RANGE) {
			return OUT_OF_RANGE;
		}

		long n2 = getFromDoublePrecision(d);
		if (n2 == OUT_OF_RANGE) {
			//
			// Transcode n into floating-point
			//
			double d1 = toDoublePrecision(n);
			//
			// Try addition with floating-points.
			//
			// Precision might be lost in return of
			// a result inside the range. 
			//
			return getFromDoublePrecision(d1 + d);
		}

		//
		// General case. Addition can be performed.
		//
		return sum(n, n2);
	}

	private static long rangeCheckedShift(long n, int i) {
		int exponent = Long.numberOfTrailingZeros(Long.highestOneBit(n));

		if (exponent > FRACTIONAL_WIDTH - i + 1) {
			return OUT_OF_RANGE;
		}

		return n << i;
	}

	public static long shift(long n, int i) {
		if (n == 0) {
			return 0;
		}

		if (n == OUT_OF_RANGE) {
			return OUT_OF_RANGE;
		}

		if (n < 0) {
			return -rangeCheckedShift(-n, i);
		}

		return rangeCheckedShift(n, i);
	}

	private static long rangeCheckedMul(long n1, long n2) {
		// Split numbers in half
		// n1, n2 store high order
		long lo1 = n1 & 0x3FFFFFFFL;
		n1 = n1 >> 30;
		long lo2 = n2 & 0x3FFFFFFFL;
		n2 = n2 >> 30;

		long rangeCheck = (n1 >> 1) * (n2 >> 1);
		if (rangeCheck < 0) {
			return OUT_OF_RANGE;
		}

		// Multiply high order
		long t = lo1 * lo2;
		t = n1 * lo2 + (t >> 30);

		long w1 = t & 0x3FFFFFFFL;
		t = t >> 30;

		w1 = lo1 * n2 + w1;

		lo1 = (n1 * n2 + t + (w1 >> 30)) >>> 1;
		if ((lo1 >> 1) < rangeCheck) {
			return OUT_OF_RANGE;
		}

		return lo1;
	}

	/**
	 * Returns a fixed-point number backed by a 64-bit signed integer whose value is {@code (n1 * n2)}.
	 * 
	 * @param n1 fixed-point number
	 * @param n2 fixed-point number
	 * @return fixed-point number equals to n1 * n2
	 */
	public static long mul(long n1, long n2) {
		if ((n1 == OUT_OF_RANGE) || (n2 == OUT_OF_RANGE)) {
			return OUT_OF_RANGE;
		}
		int sign1 = Long.signum(n1);
		int sign2 = Long.signum(n2);

		return sign1 * sign2 * rangeCheckedMul(sign1 * n1, sign2 * n2);
	}

	//
	// transposition of rangeCheckedMul(long, long)
	//
	private static long rangeCheckedSquare(long n) {
		// Split numbers in half
		// n1, n2 store high order
		long lo = n & 0x3FFFFFFFL;
		n = n >> 30;

		long rangeCheck = (n >> 1) * (n >> 1);
		if (rangeCheck < 0) {
			return OUT_OF_RANGE;
		}

		// Multiply high order
		long t = lo * lo;
		long noXlo = n * lo;
		t = noXlo + (t >> 30);

		long w1 = t & 0x3FFFFFFFL;
		t = t >> 30;

		w1 = noXlo + w1;

		lo = (n * n + t + (w1 >> 30)) >>> 1;
		if ((lo >> 1) < rangeCheck) {
			return OUT_OF_RANGE;
		}

		return lo;
	}

	/**
	 * Returns a fixed-point number backed by a 64-bit signed integer whose value is {@code (n * n)}.
	 * @param n fixed-point number
	 * @return fixed-point number equals to n * n
	 */
	public static long square(long n) {
		if (n == OUT_OF_RANGE) {
			return OUT_OF_RANGE;
		}

		if (n < 0) {
			long absN = -n;
			return rangeCheckedSquare(absN);
		}

		return rangeCheckedSquare(n);
	}

	/**
	 * Returns the square of the sum of the fixed-point number and the floating-point number.
	 * This method is intended to deliver better result than using sum and square.
	 * 
	 * @param n fixed-point number
	 * @param d floating-point value
	 * @return a fixed-point number equals to sqr(n+d)
	 */
	public static long square(long n, double d) {

		//
		// Nothing to do except returning the result since sqr(n + 0) = sqr(n).
		//
		if (d == 0.0) {
			return square(n);
		}

		//
		// Addition is possible only when operands are equally scaled.
		// Scaling is done in order to get all bits from mantissa floating point.
		//
		int scale = 52 - Math.getExponent(d) - FRACTIONAL_WIDTH;
		if (scale > 0) {
			long mantissaBits = (1L << 52) | (Double.doubleToRawLongBits(d) & 0xFFFFFFFFFFFFFL);

			//
			// Scale could be decreased by removing trailing zeros
			// in order to minimize the computation cost.
			//
			int nbZeros = Long.numberOfTrailingZeros(mantissaBits);
			scale = scale - nbZeros;

			if (scale > 0) {
				//
				// Remove trailing zeros
				mantissaBits = mantissaBits >> nbZeros;

				int signN = Long.signum(n);
				int signD = (d < 0) ? -1 : 1;

				//
				// Remove sign. Scale and store in BigInteger.
				//
				BigInteger absN = BigInteger.valueOf(signN * n).shiftLeft(scale);
				BigInteger absD = BigInteger.valueOf(mantissaBits);
				absN = (signN == signD) ? absN.add(absD) : absN.subtract(absD);

				long square = absN.multiply(absN).shiftRight(2 * scale + FRACTIONAL_WIDTH).longValue();

				if (square < 0) {
					//
					// The result is out of range
					//
					return OUT_OF_RANGE;
				}

				return square;
			}
		}

		return square(sum(n, d));
	}

	//
	// transposition of rangeCheckedMul(long, long)
	//
	private static long rangeCheckedTwicedMul(long n1, long n2) {
		// Split numbers in half
		// n1, n2 store high order
		long lo1 = n1 & 0x3FFFFFFFL;
		n1 = n1 >> 30;
		long lo2 = n2 & 0x3FFFFFFFL;
		n2 = n2 >> 30;

		long rangeCheck = (n1 >> 1) * (n2 >> 1);
		if (rangeCheck < 0) {
			return OUT_OF_RANGE;
		}

		// Multiply high order
		long t = lo1 * lo2;
		t = n1 * lo2 + (t >> 30);

		long w1 = t & 0x3FFFFFFFL;
		t = t >> 30;

		w1 = lo1 * n2 + w1;

		lo1 = (n1 * n2 + t + (w1 >> 30));
		if ((lo1 >>> 2) < rangeCheck) {
			return OUT_OF_RANGE;
		}

		return lo1;
	}

	/**
	 * Fitted for Mandelbrot computation y(n+1) = 2 * x(n) * y(n) + y(0)
	 * 
	 * @param n1 fixed-point number
	 * @param n2 fixed-point number
	 * @return fixed-point number equals to 2 * n1 * n2
	 */
	public static long twicedMul(long n1, long n2) {
		if ((n1 == OUT_OF_RANGE) || (n2 == OUT_OF_RANGE)) {
			return OUT_OF_RANGE;
		}
		int sign1 = Long.signum(n1);
		int sign2 = Long.signum(n2);

		return sign1 * sign2 * rangeCheckedTwicedMul(sign1 * n1, sign2 * n2);
	}

	/**
	 * Averaging the two specified numbers without overflow.
	 * 
	 * @param n1 fixed-point number
	 * @param n2 fixed-point number
	 * @return fixed-point number equals (n1 + n2) / 2
	 */
	public static long average(long n1, long n2) {
		if ((n1 == OUT_OF_RANGE) || (n2 == OUT_OF_RANGE)) {
			return OUT_OF_RANGE;
		}
		int sign1 = Long.signum(n1);
		int sign2 = Long.signum(n2);

		if (sign1 != sign2) {
			return (n1 + n2) >> 1;
		}

		long halfN1 = n1 >> 1;
		long halfN2 = n2 >> 1;

		return halfN1 + halfN2 + (((n1 - (halfN1 << 1)) + (n2 - (halfN2 << 1))) >> 1);
	}

	/**
	 * Averaging the three specified numbers without overflow.
	 * 
	 * @param n1 fixed-point number
	 * @param n2 fixed-point number
	 * @param n3 fixed-point number
	 * @return fixed-point number equals (n1 + n2 + n3) / 3
	 */
	public static long average(long n1, long n2, long n3) {
		if ((n1 == OUT_OF_RANGE) || (n2 == OUT_OF_RANGE) || (n3 == OUT_OF_RANGE)) {
			return OUT_OF_RANGE;
		}
		// Bubble sort
		if (n1 < n2) {
			long n = n1;
			n1 = n2;
			n2 = n;
		}
		if (n2 < n3) {
			long n = n2;
			n2 = n3;
			n3 = n;
		}
		if (n1 < n2) {
			long n = n1;
			n1 = n2;
			n2 = n;
		}
		// n1 > n2 > n3
		int sign1 = Long.signum(n1);
		int sign2 = Long.signum(n2);
		int sign3 = Long.signum(n3);

		if (sign1 != sign3) {
			long n = n1 + n3;
			long sign = Long.signum(n);
			if (sign != sign2) {
				return (n + n2) / 3;
			}

			long thirdN = n / 3;
			long thirdN2 = n2 / 3;

			return thirdN + thirdN2 + ((n - 3 * thirdN) + (n2 - 3 * thirdN2)) / 3;
		}

		long thirdN1 = n1 / 3;
		long thirdN2 = n2 / 3;
		long thirdN3 = n3 / 3;

		return thirdN1 + thirdN2 + thirdN3 + ((n1 - 3 * thirdN1) + (n2 - 3 * thirdN2) + (n3 - 3 * thirdN3)) / 3;
	}

	/**
	 * Returns the value of hypot function of (n1, n2).
	 * 
	 * @param n1 fixed-point number
	 * @param n2 fixed-point number
	 * @return the value of hypot function of (n1, n2)
	 */
	public static double hypot(long n1, long n2) {
		if ((n1 == OUT_OF_RANGE) || (n2 == OUT_OF_RANGE)) {
			return Double.NaN;
		}
		int sign1 = Long.signum(n1);
		int sign2 = Long.signum(n2);

		long absN1 = sign1 * n1;
		long absN2 = sign2 * n2;

		if (absN1 == 0) {
			return toDoublePrecision(absN2);
		}
		if (absN2 == 0) {
			return toDoublePrecision(absN1);
		}
		if (absN1 == absN2) {
			return toDoublePrecision(absN1) * 1.4142135623730951;
		}

		double x = toDoublePrecision(absN1);
		double y = toDoublePrecision(absN2);

		return Math.sqrt(x * x + y * y);
	}

	/**
	 * Checks the validity of the number.
	 * Returns false if the value is valid; out of range value is invalid.
	 * @param n fixed-point number
	 * @return true if the value is out of range.
	 */
	public static boolean isOutOfRange(long n) {
		return n == OUT_OF_RANGE;
	}

	/**
	 * Returns true if the result of the sum is out of range.
	 * 
	 * @param n1 fixed-point number
	 * @param n2 fixed-point number
	 * @return true if the result of the sum is out of range
	 */
	public static boolean isSumOutOfRange(long n1, long n2) {
		return isOutOfRange(sum(n1, n2));
	}

	/**
	 * Returns n1 - n2; assuming that n1 and n2 are positives and in range value.
	 * 
	 * @param n1 fixed-point number
	 * @param n2 fixed-point number
	 * @return n1 - n2
	 */
	public static long unsafeDiff(long n1, long n2) {
		return n1 - n2;
	}

	/**
	 * Returns the constant <i>e</i>.
	 * 
	 * @return fixed-point number equals to e
	 */
	public static long e() {
		return 6267931151224907085L;
	}

	/**
	 * Returns the constant <i>pi</i>.
	 * 
	 * @return fixed-point number equals to pi
	 */
	public static long pi() {
		return 7244019458077122842L;
	}
}
