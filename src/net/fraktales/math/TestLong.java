package net.fraktales.math;

import java.math.BigDecimal;
import java.math.BigInteger;



public class TestLong {

	private static void compare(String s1, ExtendedPrecisionNumber actual, String s2, ExtendedPrecisionNumber expected) {
		System.out.println(s1 + actual);
		System.out.println(s2 + expected);

		if (expected.compareTo(actual) == 0) {
			System.out.println("Result [OK]");
		}
		else {
			System.out.println("Result [Warning]");
		}
		System.out.println("       ==========================================================");
	}

	private static void testProduct() {
		System.out.println("========================        testProduct         ========================");
		ExtendedPrecisionNumber n0 = ExtendedPrecisionNumber.valueOf("3.12");
		System.out.println("n0      : " + n0);
		compare("n0*1    : ", n0.mul(ExtendedPrecisionNumber.valueOf("1")), "expected: ", n0);
		compare("1*n0    : ", ExtendedPrecisionNumber.valueOf("1").mul(n0), "expected: ", n0);
		ExtendedPrecisionNumber n1 = ExtendedPrecisionNumber.valueOf("9.9");
		System.out.println("n1      : " + n1);
		compare("n0*n1   : ", n0.mul(n1), "expected: ", ExtendedPrecisionNumber.valueOf("30.888"));
		compare("n1*n0   : ", n1.mul(n0), "expected: ", ExtendedPrecisionNumber.valueOf("30.888"));
		compare("n0*n0   : ", n0.mul(n0), "expected: ", ExtendedPrecisionNumber.valueOf("9.7344"));
		compare("sqr(n0) : ", n0.square(), "expected: ", ExtendedPrecisionNumber.valueOf("9.7344"));
		ExtendedPrecisionNumber n2 = ExtendedPrecisionNumber.valueOf("0.9999999999999999999");
		System.out.println("n2      : " + n2);
		double d0 = Math.scalb(0.999999999999999, -15);
		System.out.println("d0      : " + d0);
		System.out.println("n2*d0   : " + n2.mul(d0));
		ExtendedPrecisionNumber n3 = ExtendedPrecisionNumber.valueOf("-1.7473498072082414584");
		System.out.println("n3      : " + n3);
		double d1 = 0.00000000000000000007;
		System.out.println("d1      : " + d1);
		System.out.println("n3*d1   : " + n3.mul(d1));
		ExtendedPrecisionNumber n4 = ExtendedPrecisionNumber.valueOf("0.00229680676053661051");
		System.out.println("n4      : " + n4);
		double d2 = 0.00000000000000005;
		System.out.println("d2      : " + d2);
		System.out.println("n4*d2   : " + n4.mul(d2));
		System.out.println("       ======================== end test ========================");
	}

	private static void testHalvesAndTwices() {
		System.out.println("========================    testHalvesAndTwices     ========================");
		ExtendedPrecisionNumber n0 = ExtendedPrecisionNumber.valueOf("15.12");
		compare("n0/2    : ", n0.halve(), "expected: ", ExtendedPrecisionNumber.valueOf("7.56"));
		compare("n0*2    : ", n0.twice(), "expected: ", ExtendedPrecisionNumber.valueOf("30.24"));
		compare("(n0*2)/2: ", n0.twice().halve(), "expected: ", n0);
		compare("(n0/2)*2: ", n0.halve().twice(), "expected: ", n0);
		ExtendedPrecisionNumber n1 = ExtendedPrecisionNumber.valueOf("-15.12");
		compare("n1/2    : ", n1.halve(), "expected: ", ExtendedPrecisionNumber.valueOf("-7.56"));
		compare("n1*2    : ", n1.twice(), "expected: ", ExtendedPrecisionNumber.valueOf("-30.24"));
		compare("(n1*2)/2: ", n1.twice().halve(), "expected: ", n1);
		compare("(n1/2)*2: ", n1.halve().twice(), "expected: ", n1);
		ExtendedPrecisionNumber n2 = ExtendedPrecisionNumber.valueOf("1.7473498072082414584");
		compare("n2/2    : ", n2.halve(), "expected: ", ExtendedPrecisionNumber.valueOf("0.8736749036041207292"));
		compare("n2*2    : ", n2.twice(), "expected: ", ExtendedPrecisionNumber.valueOf("3.4946996144164829168"));
		compare("(n2*2)/2: ", n2.twice().halve(), "expected: ", n2);
		compare("(n2/2)*2: ", n2.halve().twice(), "expected: ", n2);
		ExtendedPrecisionNumber n3 = ExtendedPrecisionNumber.valueOf("-1.7473498072082414584");
		compare("n3/2    : ", n3.halve(), "expected: ", ExtendedPrecisionNumber.valueOf("-0.8736749036041207292"));
		compare("n3*2    : ", n3.twice(), "expected: ", ExtendedPrecisionNumber.valueOf("-3.4946996144164829168"));
		compare("(n3*2)/2: ", n3.twice().halve(), "expected: ", n3);
		compare("(n3/2)*2: ", n3.halve().twice(), "expected: ", n3);
		System.out.println("       ======================== end test ========================");
	}

	private static void testDivisionPositiveNumber() {
		System.out.println("======================== testDivisionPositiveNumber ========================");
		ExtendedPrecisionNumber n = ExtendedPrecisionNumber.valueOf("15.12");
		System.out.println("n       : " + n);
		compare("n/1     : ", n.div(1), "expected: ", ExtendedPrecisionNumber.valueOf("15.12"));
		compare("n/2     : ", n.div(2), "expected: ", ExtendedPrecisionNumber.valueOf("7.56"));
		compare("n/3     : ", n.div(3), "expected: ", ExtendedPrecisionNumber.valueOf("5.04"));
		compare("n/4     : ", n.div(4), "expected: ", ExtendedPrecisionNumber.valueOf("3.78"));
		compare("n/5     : ", n.div(5), "expected: ", ExtendedPrecisionNumber.valueOf("3.024"));
		compare("n/6     : ", n.div(6), "expected: ", ExtendedPrecisionNumber.valueOf("2.52"));
		compare("n/7     : ", n.div(7), "expected: ", ExtendedPrecisionNumber.valueOf("2.16"));
		compare("n/8     : ", n.div(8), "expected: ", ExtendedPrecisionNumber.valueOf("1.89"));
		compare("n/9     : ", n.div(9), "expected: ", ExtendedPrecisionNumber.valueOf("1.68"));
		compare("n/10    : ", n.div(10), "expected: ", ExtendedPrecisionNumber.valueOf("1.512"));
		System.out.println("       ======================== end test ========================");
	}

	private static void testDivisionNegativeNumber() {
		System.out.println("======================== testDivisionNegativeNumber ========================");
		ExtendedPrecisionNumber n = ExtendedPrecisionNumber.valueOf("-15.12");
		System.out.println("n       : " + n);
		compare("n/1     : ", n.div(1), "expected: ", ExtendedPrecisionNumber.valueOf("-15.12"));
		compare("n/2     : ", n.div(2), "expected: ", ExtendedPrecisionNumber.valueOf("-7.56"));
		compare("n/3     : ", n.div(3), "expected: ", ExtendedPrecisionNumber.valueOf("-5.04"));
		compare("n/4     : ", n.div(4), "expected: ", ExtendedPrecisionNumber.valueOf("-3.78"));
		compare("n/5     : ", n.div(5), "expected: ", ExtendedPrecisionNumber.valueOf("-3.024"));
		compare("n/6     : ", n.div(6), "expected: ", ExtendedPrecisionNumber.valueOf("-2.52"));
		compare("n/7     : ", n.div(7), "expected: ", ExtendedPrecisionNumber.valueOf("-2.16"));
		compare("n/8     : ", n.div(8), "expected: ", ExtendedPrecisionNumber.valueOf("-1.89"));
		compare("n/9     : ", n.div(9), "expected: ", ExtendedPrecisionNumber.valueOf("-1.68"));
		compare("n/10    : ", n.div(10), "expected: ", ExtendedPrecisionNumber.valueOf("-1.512"));
		System.out.println("       ======================== end test ========================");
	}

	private static void testAssociativity() {
		System.out.println("========================      testAssociativity     ========================");
		ExtendedPrecisionNumber n0 = ExtendedPrecisionNumber.valueOf("1.779882");
		ExtendedPrecisionNumber n1 = ExtendedPrecisionNumber.valueOf("0.00002");
		ExtendedPrecisionNumber expected = ExtendedPrecisionNumber.valueOf("0.00007119528");
		System.out.println("n0       : " + n0);
		System.out.println("n1       : " + n1);
		compare("(2*n0)*n1: ", n0.twice().mul(n1), "expected : ", expected);
		compare("(2*n1)*n0: ", n1.twice().mul(n0), "expected : ", expected);
	}

	private static void testConversion() {
		System.out.println("========================       testConversion       ========================");
		double d0 = 0.0;
		System.out.println("d0       : " + d0);
		System.out.println("n0       : " + ExtendedPrecisionNumber.valueOf(d0));
		double d1 = 0.125;
		System.out.println("d1       : " + d1);
		System.out.println("n1       : " + ExtendedPrecisionNumber.valueOf(d1));
		double d2 = 4.125;
		System.out.println("d2       : " + d2);
		System.out.println("n2       : " + ExtendedPrecisionNumber.valueOf(d2));
	}

	private static void testRandomSquare() {
		String[] randomValues = { "1.240277752943975",
				"1.2889978406791853",
				"1.1912663271420052",
				"1.8977937306851953",
				"1.9566967559586166",
				"0.8380547147343795",
				"1.8710934286554624",
				"0.2005561002502676",
				"1.8272024946403203",
				"1.3269558185070829" };
		for (int i = 0; i < 10; i++) {
			BigDecimal d = new BigDecimal(randomValues[i]);
			BigDecimal dXd = d.multiply(d);
			ExtendedPrecisionNumber n = ExtendedPrecisionNumber.valueOf(d);
			System.out.println("n        : " + n);
			compare("sqr(n)  : ", n.square(), "expected: ", ExtendedPrecisionNumber.valueOf(dXd));
		}
	}

	private static void fixedPointConstants() {
		BigDecimal offset = BigDecimal.valueOf(FixedPointNumber64.getFromDecimal("1"));

		//
		// --- Neper
		//
		BigDecimal unscaledValue = BigDecimal.valueOf(FixedPointNumber64.e());
		System.out.println("FixedP: " + unscaledValue.divide(offset));
		System.out.println("Exact : 2.718281828459045235 [ 18 decimals ]");
		System.out.println("Double: " + Math.E);
		System.out.println("toStr : " + FixedPointNumber64.toString(FixedPointNumber64.e()));

		//
		// --- PI
		//
		unscaledValue = BigDecimal.valueOf(FixedPointNumber64.pi());
		System.out.println("FixedP: " + unscaledValue.divide(offset));
		System.out.println("Exact : 3.141592653589793238 [ 18 decimals ]");
		System.out.println("Double: " + Math.PI);
		System.out.println("toStr : " + FixedPointNumber64.toString(FixedPointNumber64.pi()));
	}

	private static void fixedPointSquare() {
		System.out.println("========================        square(n, d)        ========================");
		String decimal = "-1.7473498072082414584";
		long n = FixedPointNumber64.getFromDecimal(decimal);
		double d1 = 0.00000000000000000006;
		System.out.println("sqr(" + decimal + " + " + d1 + ")");
		System.out.println("square(n, d)=" + FixedPointNumber64.toString(FixedPointNumber64.square(n, d1)));
		System.out.println("   basic_ops=" + FixedPointNumber64.toString(FixedPointNumber64.square(FixedPointNumber64.sum(n, d1))));
		double d2 = 0.00000000000000000009;
		System.out.println("sqr(" + decimal + " + " + d2 + ")");
		System.out.println("square(n, d)=" + FixedPointNumber64.toString(FixedPointNumber64.square(n, d2)));
		System.out.println("   basic_ops=" + FixedPointNumber64.toString(FixedPointNumber64.square(FixedPointNumber64.sum(n, d2))));
		double d3 = 0.00000000000000000021;
		System.out.println("sqr(" + decimal + " + " + d3 + ")");
		System.out.println("square(n, d)=" + FixedPointNumber64.toString(FixedPointNumber64.square(n, d3)));
		System.out.println("   basic_ops=" + FixedPointNumber64.toString(FixedPointNumber64.square(FixedPointNumber64.sum(n, d3))));
		double d4 = 0.00000000000000000024;
		System.out.println("sqr(" + decimal + " + " + d4 + ")");
		System.out.println("square(n, d)=" + FixedPointNumber64.toString(FixedPointNumber64.square(n, d4)));
		System.out.println("   basic_ops=" + FixedPointNumber64.toString(FixedPointNumber64.square(FixedPointNumber64.sum(n, d4))));
		double d5 = 0.00000000000000000036;
		System.out.println("sqr(" + decimal + " + " + d5 + ")");
		System.out.println("square(n, d)=" + FixedPointNumber64.toString(FixedPointNumber64.square(n, d5)));
		System.out.println("   basic_ops=" + FixedPointNumber64.toString(FixedPointNumber64.square(FixedPointNumber64.sum(n, d5))));
	}
	
	private static void fixedPointAverage() {
		System.out.println("========================  Average without overflow  ========================");
		long n0 = FixedPointNumber64.getFromDecimal("-1");
		long n1 = FixedPointNumber64.e();
		long n2 = FixedPointNumber64.pi();
		long n3 = FixedPointNumber64.getFromDecimal("3.9");
		long n4 = FixedPointNumber64.getFromDecimal("-3.1");
		System.out.println("n0          : " + n0);
		System.out.println("n1          : " + n1);
		System.out.println("n2          : " + n2);
		System.out.println("n3          : " + n3);
		System.out.println("n4          : " + n4);
		System.out.println("(n1+n2)/2   : " + FixedPointNumber64.average(n1, n2));
		System.out.println("(n1+n2)/2   : " + BigInteger.valueOf(n1).add(BigInteger.valueOf(n2)).shiftRight(1));
		System.out.println("(n1+n2+n3)/3: " + FixedPointNumber64.average(n1, n2, n3));
		System.out.println("(n1+n2+n3)/3: " + BigInteger.valueOf(n1).add(BigInteger.valueOf(n2)).add(BigInteger.valueOf(n3)).divide(BigInteger.valueOf(3)));
		System.out.println("(n0+n2+n3)/3: " + FixedPointNumber64.average(n0, n2, n3));
		System.out.println("(n0+n2+n3)/3: " + BigInteger.valueOf(n0).add(BigInteger.valueOf(n2)).add(BigInteger.valueOf(n3)).divide(BigInteger.valueOf(3)));
		System.out.println("(n0+n2+n4)/3: " + FixedPointNumber64.average(n0, n2, n4));
		System.out.println("(n0+n2+n4)/3: " + BigInteger.valueOf(n0).add(BigInteger.valueOf(n2)).add(BigInteger.valueOf(n4)).divide(BigInteger.valueOf(3)));
	}

	public static void main(String[] args) {
		testProduct();
		testHalvesAndTwices();
		testDivisionPositiveNumber();
		testDivisionNegativeNumber();
		testAssociativity();
		testConversion();
		testRandomSquare();
		fixedPointConstants();
		fixedPointSquare();
		fixedPointAverage();
	}
}
