package net.fraktales.math;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * The number of composed two integers; a signed 32-bit integer which represents the integer part and a signed 64-bit integer which represents the fractional part.
 * The sign of the number is zero if sign(I)==sign(F)==0.<br>
 * The sign of the number is equal to sign(F) if sign(I)==0.<br>
 * The sign of the number is equal to sign(I) in the other cases, sign(F) is normalized to be equal to sign(I).<br>
 * <code>n = [ sign(I) 31 ] [integer part 30 29 ... 0 ] [ sign(F) 63 ] [ fractional part 62 61 ... 0 ]</code>
 * 
 * @author Pascal Ollive
 * @version 1.0.13
 * @since Jun 11, 2018
 */
public final class ExtendedPrecisionNumber extends Number implements Comparable<ExtendedPrecisionNumber> {

	/**
	 * Serial UID.
	 */
	private static final long serialVersionUID = 200552774397370301L;

	/**
	 * Fractional part number of bits.
	 */
	private static final int FRACTIONAL_WIDTH = 63;

	/**
	 * Fixed-point representation of ZERO.
	 */
	private static final ExtendedPrecisionNumber ZERO = new ExtendedPrecisionNumber(0, 0);

	/**
	 * Offset 1 << 64 used for the creation of the number from a BigDecimal instance.
	 */
	private static final BigDecimal OFFSET = new BigDecimal("18446744073709551616");

	/**
	 * Number corresponding to the 63 first bit enabled stored into a {@link BigInteger}.
	 */
	private static final BigInteger BIG_INT_FRACT_MASK = BigInteger.valueOf(Long.MAX_VALUE);

	/**
	 * Fixed-point number integer part.
	 */
	private final int integerPart;

	/**
	 * Fixed-point number fractional part. 0x8000000000000000 value is reserved for future use.
	 */
	private final long fractionalPart;

	/**
	 * Internal constructor. Integer part and fractional are assumed to have the same sign; zero is compliant with minus and plus sign.
	 * 
	 * @param i integer part
	 * @param f fractional part 
	 */
	ExtendedPrecisionNumber(int i, long f) {
		integerPart = i;
		fractionalPart = f;
	}

	/**
	 * Creates a fixed-point number with 63 decimal digits from the specified decimal string representation.
	 * 
	 * @param s decimal representation of the number
	 * @return fixed-point number
	 */
	public static ExtendedPrecisionNumber valueOf(String s) {
		return valueOf(new BigDecimal(s));
	}

	/**
	 * Creates a fixed-point number with 63 decimal digits from the specified big decimal value.
	 * 
	 * @param d big decimal value
	 * @return fixed-point number
	 */
	public static ExtendedPrecisionNumber valueOf(BigDecimal d) {
		//
		// Get 64 decimal digits.
		//
		BigInteger bigInt = d.multiply(OFFSET).toBigInteger();
		if (bigInt.equals(BigInteger.ZERO)) {
			//
			// This decimal is too small to be represented by a number.
			// Returns the zero fixed-point representation available in cache.
			//
			return ZERO;
		}

		//
		// Round to 63 decimal digits <=> (x + 1) / 2
		//
		int sign = bigInt.signum();
		bigInt = bigInt.abs();
		bigInt = bigInt.add(BigInteger.ONE).shiftRight(1);

		return new ExtendedPrecisionNumber(sign * bigInt.shiftRight(FRACTIONAL_WIDTH).intValue(),
				sign * bigInt.and(BIG_INT_FRACT_MASK).longValue());
	}

	/**
	 * Creates a fixed-point number with 63 decimal digits from the specified floating-point.
	 * 
	 * @param d floating-point value
	 * @return fixed-point number
	 */
	public static ExtendedPrecisionNumber valueOf(double d) {
		int exponent = Math.getExponent(d);
		if (exponent < -FRACTIONAL_WIDTH - 1) {
			//
			// This floating-point is too small to be represented by a number.
			// Returns the zero fixed-point representation available in cache.
			//
			return ZERO;
		}

		int intPart = 0;
		long fractPart = 0;

		if (exponent < -1) {
			fractPart = Math.round(Math.scalb(d, FRACTIONAL_WIDTH));
		}
		else if (exponent < 0) {
			//
			// Rounding might produce overflow this is why the shift is only 62 (63 - 1)
			//
			fractPart = Math.round(Math.scalb(d, FRACTIONAL_WIDTH - 1)) << 1;
		}
		else {
			int sign = (Math.signum(d) == -1.0 ? -1 : 1);
			int shift = FRACTIONAL_WIDTH - 1 - exponent;
			long mask = (1L << shift) - 1;
			fractPart = sign * Math.round(Math.scalb(d, shift));
			intPart = sign * ((int) (fractPart >> shift));
			fractPart = sign * ((fractPart & mask) << (exponent + 1));
		}

		return new ExtendedPrecisionNumber(intPart, fractPart);
	}

	/**
	 * Creates a fixed-point number with 63 decimal digits from the specified integer.
	 * 
	 * @param i integer value
	 * @return fixed-point number
	 */
	public static ExtendedPrecisionNumber valueOf(int i) {
		if (i == 0) {
			//
			// Returns the zero fixed-point representation available in cache.
			//
			return ZERO;
		}

		return new ExtendedPrecisionNumber(i, 0);
	}

	public static boolean isSumLessThan(ExtendedPrecisionNumber n1, ExtendedPrecisionNumber n2, int n3) {
		int intPart = n1.integerPart + n2.integerPart;

		if (n1.fractionalPart > Long.MAX_VALUE - n2.fractionalPart) {
			intPart = intPart + 1;
		}

		return intPart < n3;
	}

	/**
	 * Returns the signum value of the number.
	 * The signum of the number is zero if sign(I)==sign(F)==0.<br>
	 * The signum of the number is equal to sign(F) if sign(I)==0.<br>
	 * The signum of the number is equal to sign(I) in the other cases, sign(F) is normalized to be equal to sign(I).<br>
	 * 
	 * @return -1 (negative), 0 (zero), 1 (positive)
	 */
	public int getSignum() {
		int sign = Integer.signum(integerPart);
		if (sign == 0) {
			sign = Long.signum(fractionalPart);
		}

		return sign;
	}

	private static ExtendedPrecisionNumber createNormalizedNumber(int i, long f) {
		return createNormalizedNumber(i, f, 0);
	}

	private static ExtendedPrecisionNumber createNormalizedNumber(int i, long f, int scale) {
		ExtendedPrecisionNumber unscaledNumber = null;

		if ((i == 0) || (f == 0)) {
			unscaledNumber = new ExtendedPrecisionNumber(i, f);
		}
		else {
			int intSign = Integer.signum(i);
			int fractSign = Long.signum(f);
			if (intSign == fractSign) {
				unscaledNumber = new ExtendedPrecisionNumber(i, f);
			}
			else {
				// f < 0 <=> modular sum (since Long.MIN_VALUE is negative too)
				// the most significant bit is reported to integer part
				unscaledNumber = new ExtendedPrecisionNumber(i + fractSign, Long.MIN_VALUE + f);
			}
		}

		if (scale == 0) {
			return unscaledNumber;
		}

		final int sign = unscaledNumber.getSignum();
		long absInt = sign * unscaledNumber.integerPart;
		if (scale < FRACTIONAL_WIDTH) {
			long mask = ((1L << scale) - 1);

			return new ExtendedPrecisionNumber((int) (sign * (absInt >> scale)),
					sign * (((absInt & mask) << (FRACTIONAL_WIDTH - scale)) | ((sign * unscaledNumber.fractionalPart) >> scale)));
		}
		else if (scale > FRACTIONAL_WIDTH) {
			int fractScale = scale - FRACTIONAL_WIDTH;
			// Rounding
			long epsilon = 1L << (fractScale - 1);
			return new ExtendedPrecisionNumber(0, sign * ((absInt + epsilon) >> fractScale));
		}
		else {
			// Rounding with fractional part most significant bit
			return new ExtendedPrecisionNumber(0, sign * (absInt + (unscaledNumber.fractionalPart >> (FRACTIONAL_WIDTH - 1))));
		}
	}

	private static long convertFromUnsigned(long n) {
		return (n + 1) >>> 1;
	}

	/**
	 * Product of fractional parts based on multiply high algorithm.
	 * <p>
	 * Note: Careful the value returned shall be considered as unsigned.
	 * </p>
	 * 
	 * @param f1 unsigned fractional part
	 * @param f2 unsigned fractional part
	 * @return 64-bit result
	 */
	private static long rangeCheckedMulf(long f1, long f2) {

		// Split numbers in half
		// f1, f2 store high order
		long lo1 = f1 & 0x7FFFFFFFL;
		f1 = f1 >> 31;
		long lo2 = f2 & 0x7FFFFFFFL;
		f2 = f2 >> 31;

		// Multiply high order
		long t = lo1 * lo2;
		t = f1 * lo2 + (t >> 31);

		long w1 = t & 0x7FFFFFFFL;
		t = t >> 31;

		w1 = lo1 * f2 + w1;

		return f1 * f2 + t + (w1 >> 31);
	}

	private static long mulf(long f1, long f2) {
		int sign1 = Long.signum(f1);
		int sign2 = Long.signum(f2);

		return sign1 * sign2 * convertFromUnsigned(rangeCheckedMulf(sign1 * f1, sign2 * f2));
	}

	//
	// @param paramInOut
	//	paramInOut integer part of number a
	//  paramInOut fractional part of number b
	//
	private static void setMulCarryAndRemainder(MutableNumber paramInOut) {
		int intSign = Integer.signum(paramInOut.integerPart);
		int fractSign = Long.signum(paramInOut.fractionalPart);

		int n1 = intSign * paramInOut.integerPart;
		long n2 = fractSign * paramInOut.fractionalPart;

		long hi2 = n2 >> 32;
		long lo2 = n2 & 0xFFFFFFFFL;

		long t = n1 * lo2;
		n2 = t & 0xFFFFFFFFL;
		t = t >> 32;

		t = n1 * hi2 + t;

		n1 = (int) (t >> 32);
		n2 = (t << 32) + n2;

		int productSign = intSign * fractSign;
		paramInOut.integerPart = productSign * ((int) ((n1 << 1) | (n2 >>> 63)));
		paramInOut.fractionalPart = productSign * (n2 & 0x7FFFFFFFFFFFFFFFL);
	}

	/**
	 * Returns true if the sum overflows.
	 * 
	 * @param fractPart1Sign sign of the first fractional part
	 * @param fractPart1 first fractional part
	 * @param fractPart2 second fractional part
	 * @return true if the sum overflows
	 */
	private static boolean isFractSumOverflow(int fractPart1Sign, long fractPart1, long fractPart2) {
		return (fractPart1Sign == Long.signum(fractPart2)) && (fractPart1Sign * fractPart1 > Long.MAX_VALUE - fractPart1Sign * fractPart2);
	}

	/**
	 * Returns a ExtendedPrecisionNumber whose value is {@code (this * n)}.
	 * 
	 * @param n the value to multiply
	 * @return the product of {@code (this * n)}
	 */
	public ExtendedPrecisionNumber mul(ExtendedPrecisionNumber n) {

		// (a, b) x (c, d) = (ac + carry(bc + ad), remainder(bc + ad) + bd)

		long remainder = mulf(fractionalPart, n.fractionalPart);

		if ((integerPart == 0) && (n.integerPart == 0)) {
			// 0.ddd x 0.ddd = 0.ddd
			return new ExtendedPrecisionNumber(0, remainder);
		}

		MutableNumber paramInOut = new MutableNumber(integerPart, n.fractionalPart);
		// dirty way : one instance
		// Clean way : copy of the parameters + 2 instances
		setMulCarryAndRemainder(paramInOut);
		int carry = paramInOut.integerPart;

		int sign = Long.signum(remainder);
		if (isFractSumOverflow(sign, remainder, paramInOut.fractionalPart)) {
			//
			// Overflow handling
			//
			carry = carry + sign;
			remainder = Long.MIN_VALUE + remainder + paramInOut.fractionalPart;
		}
		else {
			remainder = remainder + paramInOut.fractionalPart;
		}

		paramInOut.integerPart = n.integerPart;
		paramInOut.fractionalPart = fractionalPart;
		setMulCarryAndRemainder(paramInOut);

		carry = carry + paramInOut.integerPart;
		sign = Long.signum(remainder);
		if (isFractSumOverflow(sign, remainder, paramInOut.fractionalPart)) {
			//
			// Overflow handling
			//
			carry = carry + sign;
			remainder = Long.MIN_VALUE + remainder + paramInOut.fractionalPart;
		}
		else {
			remainder = remainder + paramInOut.fractionalPart;
		}

		return createNormalizedNumber((integerPart * n.integerPart) + carry, remainder);
	}

	/**
	 * Returns a ExtendedPrecisionNumber whose value is {@code (this * this)}.
	 * <p>
	 * Note: Results are more accurate than {@link #mul(ExtendedPrecisionNumber)}
	 * since computations have been optimized.
	 * </p>
	 * 
	 * @return the product of {@code (this * this)}
	 */
	public ExtendedPrecisionNumber square() {

		// (a, b) x (a, b) = (a*a + carry(2*a*b), remainder(2*a*b) + b*b)

		// remainder stores the absolute value of fractionalPart
		long remainder = Long.signum(fractionalPart) * fractionalPart;
		// remainder squared
		remainder = rangeCheckedMulf(remainder, remainder);

		if (integerPart == 0) {
			// 0.ddd x 0.ddd = 0.ddd
			return new ExtendedPrecisionNumber(0, convertFromUnsigned(remainder));
		}

		//
		// remainder has 64-bit precision so all the computations below are doubled.
		// the final result will be halved and rounded.
		// ==> one more bit precision
		//

		// quadruple the result returned by setMulCarryAndRemainder.
		// 2 * ( 2 * a * b)
		MutableNumber paramInOut = new MutableNumber(4 * integerPart, fractionalPart);
		// dirty way : one instance
		// Clean way : copy of the parameters + 2 instances
		setMulCarryAndRemainder(paramInOut);

		//
		// Get the first bit and move it to the carry
		//
		int carry = (int) (remainder >>> 63);
		//
		// Keep the following bits into the remainder
		//
		remainder = remainder & 0x7FFFFFFFFFFFFFFFL;

		carry = carry + paramInOut.integerPart;

		// int sign = Long.signum(remainder); equals to 1.
		// (isFractSumOverflow(1, remainder, paramInOut.fractionalPart))
		if ((Long.signum(paramInOut.fractionalPart) == 1) && (remainder > Long.MAX_VALUE - paramInOut.fractionalPart)) {
			//
			// Overflow handling
			//
			carry = carry + 1;
			remainder = Long.MIN_VALUE + remainder + paramInOut.fractionalPart;
		}
		else {
			remainder = remainder + paramInOut.fractionalPart;
		}

		//
		// Halving result
		//
		return createNormalizedNumber(2 * integerPart * integerPart + carry, remainder).halve();
	}

	public static ExtendedPrecisionNumber square(ExtendedPrecisionNumber n, double d) {

		//
		// Nothing to do except returning the result since sqr(n + 0) = sqr(n).
		//
		if (d == 0.0) {
			return n.square();
		}

		//
		// Addition is possible only when operands are equally scaled.
		// Scaling is done in order to get all bits from mantissa floating point.
		//
		int scale = 52 - Math.getExponent(d) - FRACTIONAL_WIDTH;
		if (scale > 0) {
			long mantissaBits = (1L << 52) | (Double.doubleToRawLongBits(d) & 0xFFFFFFFFFFFFFL);

			//
			// Scale could be decreased by removing trailing zeros
			// in order to minimize the computation cost.
			//
			int nbZeros = Long.numberOfTrailingZeros(mantissaBits);
			scale = scale - nbZeros;

			if (scale > 0) {
				//
				// Remove trailing zeros
				mantissaBits = mantissaBits >> nbZeros;

				int signN = n.getSignum();
				int signD = (d < 0) ? -1 : 1;

				//
				// Remove sign. Scale and store in BigInteger.
				//
				BigInteger absN = BigInteger.valueOf(signN * n.integerPart).shiftLeft(FRACTIONAL_WIDTH)
						.add(BigInteger.valueOf(signN * n.fractionalPart)).shiftLeft(scale);
				BigInteger absD = BigInteger.valueOf(mantissaBits);
				absN = (signN == signD) ? absN.add(absD) : absN.subtract(absD);

				// square and round
				BigInteger square = absN.multiply(absN).shiftRight(2 * scale + FRACTIONAL_WIDTH - 1).add(BigInteger.ONE).shiftRight(1);

				return new ExtendedPrecisionNumber(square.shiftRight(FRACTIONAL_WIDTH).intValue(),
						square.and(BIG_INT_FRACT_MASK).longValue());
			}
		}

		return n.sum(valueOf(d)).square();
	}

	// goal code square(n+d)=square(n)+2nd+square(d)
	// n*d

	/**
	 * Returns a ExtendedPrecisionNumber whose value is {@code (this * d)}.
	 * 
	 * @param d the floating-point to multiply
	 * @return the product of {@code (this * d)}
	 */
	public ExtendedPrecisionNumber mul(double d) {

		if (d == 0.0) {
			return ZERO;
		}

		int exponent = Math.getExponent(d);

		if (exponent < 0) {

			// (a, b) x (c, d) = (ac + carry(bc + ad), remainder(bc + ad) + bd)
			// c = 0
			// (a, b) x (0, d) = (carry(ad), remainder(ad) + bd)

			int scale = - exponent - 1;
			// Scale mantissa in order that the most significant bit is positioned to bit 62.
			long mantissa = Math.round(Math.scalb(d, FRACTIONAL_WIDTH + scale));

			long remainder = mulf(fractionalPart, mantissa);

			if (integerPart == 0) {
				// 0.ddd x 0.ddd = 0.ddd

				if (scale == 0) {
					return new ExtendedPrecisionNumber(0, remainder);
				}

				int sign = Long.signum(remainder);
				long absRemainder = sign * remainder;
				int remainderExponent = Long.numberOfTrailingZeros(Long.highestOneBit(absRemainder));

				if (scale <= remainderExponent) {
					if (scale == 1) {
						// Scale with rounding
						return new ExtendedPrecisionNumber(0, sign * ((absRemainder + 1) >> 1));
					}

					long epsilon = 1L << (scale - 1);
					if (absRemainder >= Long.MAX_VALUE - epsilon) {
						//
						// Overflow handling
						//
						return new ExtendedPrecisionNumber(0, sign * (Long.MAX_VALUE >> scale));
					}

					return new ExtendedPrecisionNumber(0, sign * ((absRemainder + epsilon) >> scale));
				}

				return ZERO;
			}

			// integerPart != 0
			int intSign = Integer.signum(integerPart);

			int mantissaSign = Long.signum(mantissa);
			int productSign = intSign * mantissaSign;
			long absInt = intSign * integerPart;
			long absMantissa = mantissaSign * mantissa;

			//
			// Overflow handling
			//
			long msb = absInt * (absMantissa >> 32);
			long lsb = absInt * (absMantissa & 0xFFFFFFFFL);

			int carry = (int) (productSign * (msb >> 31));
			long fractPart = productSign * (((msb & 0x7FFFFFFFL) << 32) + lsb);

			int sign = Long.signum(remainder);
			if (isFractSumOverflow(sign, remainder, fractPart)) {
				//
				// Overflow handling
				//
				carry = carry + sign;
				remainder = Long.MIN_VALUE + remainder + fractPart;
			}
			else {
				remainder = remainder + fractPart;
			}

			return createNormalizedNumber(carry, remainder, scale);
		}

		return mul(valueOf(d));
	}

	public ExtendedPrecisionNumber sum(ExtendedPrecisionNumber n) {
		int intPart = integerPart + n.integerPart;
		long fractPart = fractionalPart;

		int sign = Long.signum(fractPart);
		if (isFractSumOverflow(sign, fractPart, n.fractionalPart)) {
			//
			// Overflow handling
			//
			intPart = intPart + sign;
			fractPart = Long.MIN_VALUE + fractPart + n.fractionalPart;
		}
		else {
			fractPart = fractPart + n.fractionalPart;
		}

		return createNormalizedNumber(intPart, fractPart);
	}

	public ExtendedPrecisionNumber diff(ExtendedPrecisionNumber n) {
		int intPart = integerPart - n.integerPart;
		long fractPart = fractionalPart;

		int fractSign = Long.signum(fractPart);
		if ((fractSign != Long.signum(n.fractionalPart)) && ((fractSign * fractPart) > Long.MAX_VALUE + fractSign * n.fractionalPart)) {
			//
			// Overflow handling
			//
			intPart = intPart + fractSign;
			fractPart = Long.MIN_VALUE + fractPart - n.fractionalPart;
		}
		else {
			fractPart = fractPart - n.fractionalPart;
		}

		return createNormalizedNumber(intPart, fractPart);
	}

	public ExtendedPrecisionNumber negate() {
		return new ExtendedPrecisionNumber(-integerPart, -fractionalPart);
	}

	public ExtendedPrecisionNumber twice() {
		int sign = Integer.signum(integerPart);
		if (sign == 0) {
			sign = Long.signum(fractionalPart);
		}

		int absInt = sign * integerPart;
		long absFract = sign * fractionalPart;

		if ((absFract & 0x4000000000000000L) != 0) {
			absInt = (absInt << 1) | 0x1;
			absFract = (absFract & 0x3FFFFFFFFFFFFFFFL) << 1;
		}
		else {
			absInt = absInt << 1;
			absFract = absFract << 1;
		}

		return new ExtendedPrecisionNumber(sign * absInt, sign * absFract);
	}

	public ExtendedPrecisionNumber halve() {
		final int sign = getSignum();

		long absInt = sign * integerPart;
		long absFract = sign * fractionalPart;
		// Rounding
		long epsilon = (absFract & 0x1);
		absFract = ((absInt & 0x1) << (FRACTIONAL_WIDTH - 1)) | (absFract >> 1);
		if ((absFract < Long.MAX_VALUE) || (epsilon == 0)) {
			return new ExtendedPrecisionNumber((int) (sign * (absInt >> 1)), sign * (absFract + epsilon));
		}

		return new ExtendedPrecisionNumber((int) (sign * ((absInt >> 1) + 1)), 0);
	}

	public ExtendedPrecisionNumber div(int i) {
		//
		// Integer part
		//
		int intPart = integerPart / i;

		//
		// Fractional part
		//

		// Report the remainder of the integer part
		long fractPart = integerPart - (i * intPart);
		fractPart = -1 * ((Long.MIN_VALUE / i) * fractPart);
		fractPart = fractPart + (fractionalPart / i);

		return new ExtendedPrecisionNumber(intPart, fractPart);
	}

	@Override
	public int intValue() {
		return integerPart;
	}

	@Override
	public long longValue() {
		return integerPart;
	}

	@Override
	public float floatValue() {
		return (float) doubleValue();
	}

	@Override
	public double doubleValue() {
		if (integerPart == 0) {
			return Math.scalb((double) fractionalPart, -FRACTIONAL_WIDTH);
		}
		int sign = (Integer.signum(integerPart) == -1 ? -1 : 1);
		long absInt = sign * integerPart;
		long absFract = sign * fractionalPart;
		int exponent = Long.numberOfTrailingZeros(Long.highestOneBit(absInt));

		long rawBits = (absInt << (FRACTIONAL_WIDTH - 1 - exponent)) | (absFract >> (exponent + 1));

		return sign * Math.scalb((double) rawBits, -FRACTIONAL_WIDTH + exponent + 1);
	}

	@Override
	public int compareTo(ExtendedPrecisionNumber n) {
		if (integerPart < n.integerPart) {
			return -1;
		}
		if (integerPart > n.integerPart) {
			return 1;
		}
		if (fractionalPart < n.fractionalPart) {
			return -1;
		}
		if (fractionalPart > n.fractionalPart) {
			return 1;
		}
		return 0;
	}

	@Override
	public String toString() {
		final int sign = getSignum();

		StringBuilder sb = (sign == -1 ? new StringBuilder("-") : new StringBuilder("+"));
		String fract = Long.toBinaryString(sign * fractionalPart);
		sb.append(Long.toBinaryString(sign * integerPart));
		sb.append('.');
		int nbLeadingZeros = FRACTIONAL_WIDTH - fract.length();
		for (int i = 0; i < nbLeadingZeros; i++) {
			sb.append(0);
		}
		sb.append(fract);

		return sb.toString();
	}

	/**
	 * Mutable version of ExtendedPrecisionNumber.
	 * 
	 * This companion class is used to reduce object creation cost, see {@link #ExtendedPrecisionNumber.mul(ExtendedPrecisionNumber)} method.
	 */
	private static class MutableNumber {
		private int integerPart;
		private long fractionalPart;

		private MutableNumber(int i, long f) {
			integerPart = i;
			fractionalPart = f;
		}
	}
}
