package net.fraktales.data;

import java.util.HashMap;

import net.fraktales.math.FixedPointNumber64;

public class GeneratorParameters implements Cloneable {
	private static final long DEFAULT_POS_X = FixedPointNumber64.getFromDecimal("-1.7798819997");
	private static final long DEFAULT_POS_Y = FixedPointNumber64.getFromDecimal("0");
	private static final double DEFAULT_STEP = 0.005;

	private static final ParameterValue BOOLEAN_PARAM = new ParameterValue();
	private final HashMap<String, ParameterValue> params = new HashMap<String, ParameterValue>();

	public LevelSetting level;

	protected GeneratorParameters(int frameWidth, int frameHeight, long posX, long posY, double step, LevelSetting level) {
		setParam("defaultEngine", true);
		setParam("width", frameWidth);
		setParam("height", frameHeight);
		setParam("x", posX);
		setParam("y", posY);
		setParam("step", step);
		this.level = level;
		setParam("aaEnabled", false);
		setParam("bigFileEnabled", false);
	}

	public GeneratorParameters(GeneratorParameters p) {
		this(p.getIntegerValue("width"), p.getIntegerValue("height"),
				p.getFixedPointValue("x"), p.getFixedPointValue("y"), p.getFloatValue("step"),
				p.level);
	}

	public GeneratorParameters() {
		this(0, 0, DEFAULT_POS_X, DEFAULT_POS_Y, DEFAULT_STEP, LevelSetting.BASIC);
	}

	//
	// Boolean
	//

	public boolean isParamEnabled(String parameter) {
		ParameterValue value = params.get(parameter);

		if (value != null) {
			return true;
		}
		return false;
	}

	public void setParam(String parameter, boolean value) {
		if (value) {
			params.put(parameter, BOOLEAN_PARAM);
		}
		else {
			params.remove(parameter);
		}
	}

	//
	// Integer
	//

	public int getIntegerValue(String parameter) {
		ParameterValue value = params.get(parameter);

		if (value != null) {
			return value.intValue();
		}
		return 0;
	}

	public void setParam(String parameter, int value) {
		params.put(parameter, new ParameterValue(value));
	}

	//
	// Floating-point value
	//

	public double getFloatValue(String parameter) {
		ParameterValue value = params.get(parameter);

		if (value != null) {
			return value.doubleValue();
		}
		return 0.0;
	}

	public void setParam(String parameter, double value) {
		params.put(parameter, new ParameterValue(value));
	}

	//
	// Fixed-point value
	//

	public long getFixedPointValue(String parameter) {
		ParameterValue value = params.get(parameter);

		if (value != null) {
			return value.fixedPointValue();
		}
		return 0;
	}

	public void setParam(String parameter, long value) {
		params.put(parameter, new ParameterValue(value));
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("Param: ");
		StringBuilder booleanParams = new StringBuilder();
		for (String p : params.keySet()) {
			ParameterValue value = params.get(p);
			if (value == BOOLEAN_PARAM) {
				// boolean parameters are displayed lastly
				booleanParams.append(", ");
				booleanParams.append(p);
			}
			else {
				sb.append(p);
				sb.append('=');
				sb.append(value);
				sb.append(", ");
			}
		}
		sb.append("l=");
		sb.append(level);
		// add the boolean parameters to the end of the list
		sb.append(booleanParams);

		return sb.toString();
	}

	@Override
	public GeneratorParameters clone() {
		GeneratorParameters clone;
		try {
			clone = (GeneratorParameters) super.clone();
		}
		catch (CloneNotSupportedException e) {
			throw new AssertionError(e);
		}
		return clone;
	}
}
