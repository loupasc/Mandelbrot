package net.fraktales.data;

import net.fraktales.math.FixedPointNumber64;

public final class ParameterValue {

	private final Class<?> type;
	private final long value;

	private ParameterValue(Class<?> type, long value) {
		this.type = type;
		this.value = value;
	}

	public ParameterValue() {
		this(boolean.class, Long.MIN_VALUE);
	}

	public ParameterValue(int value) {
		this(int.class, value);
	}

	public ParameterValue(double value) {
		this(double.class, Double.doubleToRawLongBits(value));
	}

	public ParameterValue(long value) {
		this(FixedPointNumber64.class, value);
	}

	public double doubleValue() {
		if (type == double.class) {
			return Double.longBitsToDouble(value);
		}
		if (type == FixedPointNumber64.class) {
			return FixedPointNumber64.toDoublePrecision(value);
		}
		return value;
	}

	public int intValue() {
		if (type == double.class) {
			return (int) Double.longBitsToDouble(value);
		}
		if (type == FixedPointNumber64.class) {
			return FixedPointNumber64.toInteger(value);
		}
		return (int) value;
	}

	public long fixedPointValue() {
		if (type == double.class) {
			return FixedPointNumber64.getFromDoublePrecision(Double.longBitsToDouble(value));
		}
		if (type == FixedPointNumber64.class) {
			return value;
		}
		return FixedPointNumber64.getFromDoublePrecision(value);
	}

	@Override
	public String toString() {
		if (type == boolean.class) {
			return Boolean.toString(true);
		}
		if (type == double.class) {
			return Double.toString(doubleValue());
		}
		if (type == FixedPointNumber64.class) {
			return FixedPointNumber64.toString(value);
		}
		return Integer.toString(intValue());
	}
}
