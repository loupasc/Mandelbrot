package net.fraktales.data;

public enum LevelSetting {
	/** 10 */
	MINIMUM((short) 10, new Converter() {
		@Override
		public int levelToLuma(float level, int x, int y) {
			return (255 * Math.round(5 * level)) / 50;
		}
	}),
	/** 51 */
	LOW((short) 51, new Converter() {
		@Override
		public int levelToLuma(float level, int x, int y) {
			return Math.round(5 * level);
		}
	}),
	/** 255 */
	BASIC((short) 255, new Converter() {
		@Override
		public int levelToLuma(float level, int x, int y) {
			return (Math.round(5 * level) + (DITHER_MAP[(5 * (y % 5)) + (x % 5)] / 5)) / 5;
		}
	}),
	/** 1275 */
	STANDARD((short) 1275, new Converter() {
		@Override
		public int levelToLuma(float level, int x, int y) {
			return (Math.round(5 * level) + (DITHER_MAP[(5 * (y % 5)) + (x % 5)] / 5)) / 25;
		}
	}),
	/** 1275+ */
	STANDARD_PLUS("1275+", (short) 6375, new Converter() {
		@Override
		public int levelToLuma(float level, int x, int y) {
			return STANDARD.convert.levelToLuma(level , x, y);
		}
	}),
	/** 6375 */
	MAXIMUM((short) 6375, new Converter() {
		@Override
		public int levelToLuma(float level, int x, int y) {
			return (Math.round(5 * level) + DITHER_MAP[(5 * (y % 5)) + (x % 5)]) / 125;
		}
	});

	//
	// Non-rectangular dither tile
	//        [   4   ]
	// seed = [ 3 0 1 ]
	//        [   2   ]
	//
	// Five tiles in the following square.
	//
	private static final int[] DITHER_MAP = new int[] {
		21, 15,  0,  5, 14,
		 1,  6, 10, 22, 16,
		11, 23, 17,  2,  7,
		18,  3,  8, 12, 24,
		 9, 13, 20, 19,  4
	};

	public final String name;
	public final short count;
	private final Converter convert;

	private LevelSetting(String name, short count, Converter convert) {
		this.name = name;
		this.count = count;
		this.convert = convert;
	}

	private LevelSetting(short count, Converter convert) {
		this(Short.toString(count), count, convert);
	}

	public int getLumaFromLevel(float level, int x, int y) {
		return convert.levelToLuma(level, x, y);
	}

	private interface Converter {
		// levels to 256 greyscales
		public int levelToLuma(float level, int x, int y);
	}
}
