package net.fraktales.view;

import java.awt.Cursor;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 * Swing component used to display the generated image. This component holds a BufferedImage and it displays a hand cursor indicating the user can click.
 * 
 * @author Pascal Ollive
 * @version 1.0.8
 * @since Jun 14, 2018
 */
public class ImageComponent extends JLabel {

	/**
	 * Appease the serialization gods.
	 */
	private static final long serialVersionUID = -3291065051346327325L;

	/**
	 * Image component state.
	 */
	private boolean enabled;

	/**
	 * Unique mouse listener depending of the component state.
	 */
	private MouseListener mouseListener;

	/**
	 * Creates an ImageComponent instance with the specified BufferedImage.
	 *
	 * @param image image displayed
	 */
	public ImageComponent(BufferedImage image) {
		super(new ImageIcon(image));
		enabled = false;
		mouseListener = null;
	}

	/**
	 * Sets the unique mouse listener.
	 * 
	 * @param l mouse listener
	 */
	public void setMouseListener(MouseListener l) {
		this.mouseListener = l;
	}

	@Override
	public void setEnabled(boolean enabled) {
		if (this.enabled != enabled) {
			this.enabled = enabled;
			if (enabled) {
				setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				addMouseListener(mouseListener);
			}
			else {
				removeMouseListener(mouseListener);
				setCursor(Cursor.getDefaultCursor());
			}
		}
	}
}
