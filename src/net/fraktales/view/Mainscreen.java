package net.fraktales.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.math.BigDecimal;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;

import net.fraktales.controller.FractalManager;
import net.fraktales.data.GeneratorParameters;
import net.fraktales.data.LevelSetting;
import net.fraktales.event.GeneratorListener;
import net.fraktales.math.FixedPointNumber64;
import net.fraktales.tool.ImageUtils;

/**
 * Main graphical user interface.
 * 
 * @author POE
 * @version 1.1.0
 * @since Jun 11, 2018
 */
public class Mainscreen extends JFrame implements Runnable, WindowListener, MouseListener, GeneratorListener {
	private static final long serialVersionUID = -3769919581613269169L;
	private static final int FRAME_HEIGHT = 684;
	private static final int FRAME_WIDTH = 1024;
	private final FractalManager controller;
	private final ImageComponent imageComponent;
	private final BufferedImage image;
	private final ToolbarSettings toolbar;
	private final JButton exportToFile;
	private final JLabel logger;

	public Mainscreen(String version, FractalManager controller) {
		super("Mandelbrot-" + version);
		setIconImage(MandelIconFactory.createImage());
		this.controller = controller;
		image = new BufferedImage(FRAME_WIDTH, FRAME_HEIGHT, BufferedImage.TYPE_BYTE_GRAY);
		imageComponent = new ImageComponent(image);
		toolbar = new ToolbarSettings(controller);
		exportToFile = new JButton("Save");
		exportToFile.setEnabled(false);
		exportToFile.setToolTipText("Save displayed image to disk");
		logger = new JLabel("Welcome");
		addWindowListener(this);
	}

	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		toolbar.initialize(exportToFile);
		add(toolbar, BorderLayout.NORTH);
		imageComponent.setPreferredSize(new Dimension(FRAME_WIDTH, FRAME_HEIGHT));
		add(imageComponent, BorderLayout.CENTER);
		add(logger, BorderLayout.SOUTH);
		setResizable(false);
		pack();
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		controller.setParam("width", FRAME_WIDTH);
		controller.setParam("height", FRAME_HEIGHT);
		controller.setEventListener(this);
		setVisible(true);
		imageComponent.setMouseListener(this);
		imageComponent.setEnabled(true);
	}

	public void setUserInputEnabled(boolean enabled) {
		toolbar.setEnabled(enabled);
		imageComponent.setEnabled(enabled);
	}

	/* (non-Javadoc)
	 * @see net.fraktales.event.GeneratorListener#processingProgressUpdated(java.lang.String)
	 */
	@Override
	public void processingProgressUpdated(final String message) {
		if (SwingUtilities.isEventDispatchThread()) {
			logger.setText(message);
			if (message.equals("Start processing...")) {
				setUserInputEnabled(false);
				//
				// Hatch previous image
				//
				byte[] blackLine = new byte[FRAME_WIDTH];
				int halfFrameHeight = FRAME_HEIGHT / 2;
				int doubleFrameWidth = 2 * FRAME_WIDTH;
				byte[] output = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
				for (int j = 0; j < halfFrameHeight; j++) {
					System.arraycopy(blackLine, 0, output, j * doubleFrameWidth, FRAME_WIDTH);
				}
				repaint();
			}
		}
		else {
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					processingProgressUpdated(message);
				}
			});
		}
	}

	private static byte clamp(float intensity) {
		if (intensity > 255) {
			return (byte) 255;
		}
		if (intensity < 0) {
			return 0;
		}
		return (byte) Math.round(intensity);
	}

	private static byte getPixelValue(float[] bloc, LevelSetting s, int x, int y) {
		if (s.count >= 255) {
			float clippingValue = s.count;
			if (s == LevelSetting.STANDARD_PLUS) {
				clippingValue = LevelSetting.STANDARD.count;
				for (int i = 0; i < bloc.length; i++) {
					if (bloc[i] == LevelSetting.MAXIMUM.count) {
						bloc[i] = 0;
					}
					else if (bloc[i] > clippingValue) {
						bloc[i] = clippingValue;
					}
				}
			}
			float pixelValue = ImageUtils.convolve(bloc);
			if (pixelValue > clippingValue) {
				pixelValue = clippingValue;
			}
			else if (pixelValue < 0) {
				pixelValue = 0;
			}
			return (byte) s.getLumaFromLevel(pixelValue, x, y);
		}

		for (int i = 0; i < bloc.length; i++) {
			bloc[i] = s.getLumaFromLevel(bloc[i], x, y);
		}

		return clamp(ImageUtils.convolve(bloc));
	}

	private static byte getPixelValue(float level, LevelSetting s, int x, int y) {
		if (s != LevelSetting.STANDARD_PLUS) {
			return (byte) s.getLumaFromLevel(level, x, y);
		}
		//
		// LevelSetting.STANDARD_PLUS
		//
		if (level < LevelSetting.STANDARD.count) {
			return (byte) s.getLumaFromLevel(level, x, y);
		}
		if (level == LevelSetting.MAXIMUM.count) {
			return 0;
		}
		return (byte) 255;
	}

	/* (non-Javadoc)
	 * @see net.fraktales.event.GeneratorListener#processingCompleted(net.fraktales.data.GeneratorParameters, int, float[])
	 */
	@Override
	public void processingCompleted(final GeneratorParameters param, final int numberOfIterations, final float[] bitmap) {
		if (SwingUtilities.isEventDispatchThread()) {
			logger.setText(param.toString() + ", iterations=" + numberOfIterations);
			byte[] output = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();

			int frameWidth = param.getIntegerValue("width");
			int frameHeight = param.getIntegerValue("height");

			if (param.isParamEnabled("aaEnabled")) {

				float[] pixelBloc6x6 = ImageUtils.create6x6PixelBloc();

				int lineWidth = ImageUtils.getAntiAliasingEdgeSize(frameWidth);
				for (int j = 0; j < frameHeight; j++) {
					int offset = 2 * j * lineWidth + 2 * lineWidth + 2;
					for (int i = 0; i < frameWidth; i++) {
						ImageUtils.fillInscribedCircleIn6x6PixelBloc(lineWidth, offset, bitmap, pixelBloc6x6);

						output[FRAME_WIDTH * j + i] = getPixelValue(pixelBloc6x6, param.level, i, j);
						offset = offset + 2;
					}
				}
			}
			else {
				for (int j = 0; j < frameHeight; j++) {
					for (int i = 0; i < frameWidth; i++) {
						byte pixelValue = getPixelValue(bitmap[frameWidth * j + i], param.level, i, j);
						output[FRAME_WIDTH * j + i] = pixelValue;
					}
				}
			}

			if (!exportToFile.isEnabled()) {
				exportToFile.addActionListener(new ExportToFileAction(this, image));
				exportToFile.setEnabled(true);
			}
			setUserInputEnabled(true);
			repaint();
		}
		else {
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					processingCompleted(param, numberOfIterations, bitmap);
				}
			});
		}
	}

	/* (non-Javadoc)
	 * @see net.fraktales.event.GeneratorListener#fileExported(java.lang.String)
	 */
	@Override
	public void fileExported(final String filename) {
		if (SwingUtilities.isEventDispatchThread()) {
			setUserInputEnabled(true);
			logger.setText("Image saved in " + filename);
		}
		else {
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					fileExported(filename);
				}
			});
		}
	}

	/* (non-Javadoc)
	 * @see net.fraktales.event.GeneratorListener#processingError(java.lang.String)
	 */
	@Override
	public void processingError(final String cause) {
		if (SwingUtilities.isEventDispatchThread()) {
			setUserInputEnabled(true);
			logger.setText(cause);
		}
		else {
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					processingError(cause);
				}
			});
		}
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// Nothing to do
	}

	@Override
	public void windowClosing(WindowEvent e) {
		controller.dispose();
	}

	@Override
	public void windowClosed(WindowEvent e) {
		// Nothing to do
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// Nothing to do
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// Nothing to do
	}

	@Override
	public void windowActivated(WindowEvent e) {
		// Nothing to do
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// Nothing to do
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// Nothing to do
	}

	@Override
	public void mousePressed(MouseEvent e) {
		BigDecimal step = BigDecimal.valueOf(controller.getFloatValue("step"));

		BigDecimal dx = step.multiply(BigDecimal.valueOf(e.getX() - (FRAME_WIDTH / 2)));
		BigDecimal dy = step.multiply(BigDecimal.valueOf(e.getY() - (FRAME_HEIGHT / 2)));

		BigDecimal posX = FixedPointNumber64.toDecimal(controller.getFixedPointValue("x"));
		BigDecimal posY = FixedPointNumber64.toDecimal(controller.getFixedPointValue("y"));

		toolbar.setPosition(posX.add(dx).toPlainString(),
				posY.add(dy).toPlainString());
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// Nothing to do
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// Nothing to do
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// Nothing to do
	}
}
