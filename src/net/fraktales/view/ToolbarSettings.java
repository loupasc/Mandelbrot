package net.fraktales.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;

import net.fraktales.controller.FractalManager;
import net.fraktales.data.LevelSetting;
import net.fraktales.math.FixedPointNumber64;

public class ToolbarSettings extends JPanel {
	private static final long serialVersionUID = -5931577965648123834L;
	private final FractalManager controller;
	private final JTextField posXEditBox;
	private final JTextField posYEditBox;
	private final JTextField stepEditBox;
	private final JButton qualitySwitch;
	private final JButton levelSwitch;
	private final JButton processTrigger;
	private final JButton exportToBigFile;
	private RenderingProfile quality;

	public ToolbarSettings(FractalManager controller) {
		this.controller = controller;
		posXEditBox = new JTextField(15);
		posYEditBox = new JTextField(15);
		stepEditBox = new JTextField(15);
		quality = RenderingProfile.Q0;
		qualitySwitch = new JButton(quality.name());
		qualitySwitch.setToolTipText("Rendering quality");
		levelSwitch = new JButton("level");
		processTrigger = new JButton("Go!");
		exportToBigFile = new JButton("Big file export");
	}

	public void initialize(JComponent c) {
		add(new JLabel("X:"));
		posXEditBox.setText(FixedPointNumber64.toString(controller.getFixedPointValue("x")));
		add(posXEditBox);
		add(new JLabel("Y:"));
		posYEditBox.setText(FixedPointNumber64.toString(controller.getFixedPointValue("y")));
		add(posYEditBox);
		add(new JLabel("Step:"));
		stepEditBox.setText(Double.toString(controller.getFloatValue("step")));
		add(stepEditBox);
		qualitySwitch.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (quality == RenderingProfile.Q0) {
					quality = RenderingProfile.Q1;
					controller.setParam("aaEnabled", true);
				}
				else if (quality == RenderingProfile.Q1) {
					quality = RenderingProfile.Q2;
					controller.setParam("defaultEngine", false);
					controller.setParam("aaEnabled", false);
				}
				else if (quality == RenderingProfile.Q2) {
					quality = RenderingProfile.Q3;
					controller.setParam("aaEnabled", true);
				}
				else {
					quality = RenderingProfile.Q0;
					controller.setParam("aaEnabled", false);
					controller.setParam("defaultEngine", true);
				}

				qualitySwitch.setText(quality.name());
			}
		});
		add(qualitySwitch);
		levelSwitch.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int numberOfSettings = LevelSetting.values().length;
				int nextSettingIndex = controller.getLevelSetting().ordinal() + 1;
				if (nextSettingIndex == numberOfSettings) {
					nextSettingIndex = 0;
				}
				LevelSetting level = LevelSetting.values()[nextSettingIndex];
				levelSwitch.setText(level.name);
				controller.setLevelSetting(level);
			}
		});
		levelSwitch.setText(controller.getLevelSetting().name);
		add(levelSwitch);
		add(processTrigger);
		add(c);
		add(new JSeparator());
		add(exportToBigFile);

		processTrigger.addActionListener(new ProcessTrigger(false));
		exportToBigFile.addActionListener(new ProcessTrigger(true));
	}

	public void setPosition(String x, String y) {
		posXEditBox.setText(x);
		posYEditBox.setText(y);
	}

	private class ProcessTrigger implements ActionListener {
		private final boolean bigFileEnabled;

		public ProcessTrigger(boolean bigFileEnabled) {
			this.bigFileEnabled = bigFileEnabled;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				controller.setParam("x", FixedPointNumber64.getBoundedValueFromDecimal(posXEditBox.getText()));
			}
			catch (NumberFormatException nfe) {
				//
				// Back to the previous value
				posXEditBox.setText(FixedPointNumber64.toString(controller.getFixedPointValue("x")));
			}

			try {
				controller.setParam("y", FixedPointNumber64.getBoundedValueFromDecimal(posYEditBox.getText()));
			}
			catch (NumberFormatException nfe) {
				//
				// Back to the previous value
				posYEditBox.setText(FixedPointNumber64.toString(controller.getFixedPointValue("y")));
			}

			try {
				double value = Double.parseDouble(stepEditBox.getText());
				controller.setParam("step", value);
			}
			catch (NumberFormatException nfe) {
				//
				// Back to the previous value
				stepEditBox.setText(Double.toString(controller.getFloatValue("step")));
			}

			controller.setParam("bigFileEnabled", bigFileEnabled);
			controller.execute();
		}
	}

	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		posXEditBox.setEnabled(enabled);
		posYEditBox.setEnabled(enabled);
		stepEditBox.setEnabled(enabled);
		qualitySwitch.setEnabled(enabled);
		levelSwitch.setEnabled(enabled);
		processTrigger.setEnabled(enabled);
		exportToBigFile.setEnabled(enabled);
	}
}
