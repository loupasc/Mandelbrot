package net.fraktales.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import net.fraktales.event.GeneratorListener;

public class ExportToFileAction implements ActionListener {
	private static final String FILE_PREFIX = "mandel";
	private final GeneratorListener eventListener;
	private final BufferedImage image;
	private int count;

	public ExportToFileAction(GeneratorListener eventListener, BufferedImage image) {
		this.eventListener = eventListener;
		this.image = image;
		this.count = 0;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String filename = FILE_PREFIX + String.format("%03x", count).toUpperCase() + ".bmp";
		count = (count + 1) & 0xFFF;

		try {
			ImageIO.write(image, "bmp", new File(filename));
			eventListener.fileExported(filename);
		}
		catch (IOException exception) {
			eventListener.processingError("Cannot write " + filename + ", " + exception.getMessage());
		}
		catch (Exception exception) {
			eventListener.processingError("Cannot write " + filename + ", see error output");
			exception.printStackTrace();
		}
	}
}
