package net.fraktales.controller;

import net.fraktales.math.ExtendedPrecisionNumber;
import net.fraktales.math.FixedPointNumber64;

/**
 * Computation engine based on {@link ExtendedPrecisionNumber}.
 * 
 * 
 * @author Pascal Ollive
 * @version 1.1.0
 * @since Jun 11, 2018
 */
public class FinestMandelbrotEngine extends MandelbrotEngine {

	private final ExtendedPrecisionNumber posX;
	private final ExtendedPrecisionNumber posY;

	public FinestMandelbrotEngine(long posX, long posY, int levelMax) {
		super(posX, posY, levelMax);
		this.posX = ExtendedPrecisionNumber.valueOf(FixedPointNumber64.toDecimal(posX));
		this.posY = ExtendedPrecisionNumber.valueOf(FixedPointNumber64.toDecimal(posY));
	}

	@Override
	public float getIterationFromPoint(double shiftX, double shiftY) {
		ExtendedPrecisionNumber x0 = posX.sum(ExtendedPrecisionNumber.valueOf(shiftX));
		ExtendedPrecisionNumber y0 = posY.sum(ExtendedPrecisionNumber.valueOf(shiftY));
		ExtendedPrecisionNumber x = x0;
		ExtendedPrecisionNumber y = y0;
		ExtendedPrecisionNumber xe2 = ExtendedPrecisionNumber.square(posX, shiftX);
		ExtendedPrecisionNumber ye2 = ExtendedPrecisionNumber.square(posY, shiftY);

		int iteration = 0;

		while ((iteration < levelMax) && ExtendedPrecisionNumber.isSumLessThan(xe2, ye2, 4)) {
			y = x.twice().mul(y).sum(y0);
			x = xe2.diff(ye2).sum(x0);
			xe2 = x.square();
			ye2 = y.square();

			iteration = iteration + 1;
		}

		if (iteration == levelMax) {
			return levelMax;
		}
		return iteration + getFractionalIterationFromOrbitRadius(Math.sqrt(xe2.sum(ye2).doubleValue()));
	}

	@Override
	public boolean isResultEqualWith(double d1, double d2) {
		return false;
	}
}
