package net.fraktales.controller;



public abstract class MandelbrotEngine {
	protected final int levelMax;

	public MandelbrotEngine(long posX, long posY, int levelMax) {
		this.levelMax = levelMax;
	}

	protected float getFractionalIterationFromOrbitRadius(double orbitRadius) {
		//
		// Smooth coloring r = [2..4]
		//

		// 2**(2**1)
		if (Double.isNaN(orbitRadius) || (orbitRadius > 4.0)) {
			return 0;
		}
		// 2**(2**0.75)
		if (orbitRadius > 3.208263928999625) {
			return 0.2f;
		}
		// 2**(2**0.5)
		if (orbitRadius > 2.665144142690225) {
			return 0.4f;
		}
		// 2**(2**0.25)
		if (orbitRadius > 2.2802738806995024) {
			return 0.6f;
		}
		return 0.8f;
	}

	public abstract float getIterationFromPoint(double shiftX, double shiftY);
	
	public abstract boolean isResultEqualWith(double d1, double d2);
}
