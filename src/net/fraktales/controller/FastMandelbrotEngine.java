package net.fraktales.controller;

import net.fraktales.math.FixedPointNumber64;

/**
 * Quick computation engine based on {@link FixedPointNumber64}.
 * 
 * Include periodicity checking to improve computation speed.
 * 
 * @author Pascal Ollive
 * @version 1.1.0
 * @since Mar 30, 2020
 */
public class FastMandelbrotEngine extends MandelbrotEngine {

	private final double posX;
	private final double posY;
	private final int periodMask;

	public FastMandelbrotEngine(long posX, long posY, int levelMax) {
		super(posX, posY, levelMax);
		this.posX = FixedPointNumber64.toDoublePrecision(posX);
		this.posY = FixedPointNumber64.toDoublePrecision(posY);
		periodMask = (levelMax <= 255 ? 0xF : 0xFF);
	}

	@Override
	public float getIterationFromPoint(double shiftX, double shiftY) {
		final double x0 = posX + shiftX;
		final double y0 = posY + shiftY;

		double x = x0;
		double y = y0;

		double ckx = x0;
		double cky = y0;

		double xe2 = x0 * x0;
		double ye2 = y0 * y0;

		int iteration = 0;

		while ((iteration < levelMax) && (xe2 + ye2 < 4.)) {
			y = 2 * x * y + y0;
			x = xe2 - ye2 + x0;

			//
			// Periodicity checking
			//
			if ((x == ckx) && (y == cky)) {
				//
				// Orbit point comes back to the previous value => endless cycle
				//
				return levelMax;
			}
			if ((iteration & periodMask) == 0) {
				ckx = x;
				cky = y;
			}

			xe2 = x * x;
			ye2 = y * y;

			iteration = iteration + 1;
		}

		if (iteration == levelMax) {
			return levelMax;
		}
		return iteration + getFractionalIterationFromOrbitRadius(Math.sqrt(xe2 + ye2));
	}

	@Override
	public boolean isResultEqualWith(double d1, double d2) {
		return false;
	}
}
