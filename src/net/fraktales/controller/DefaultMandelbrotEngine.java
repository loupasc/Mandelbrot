package net.fraktales.controller;

import net.fraktales.math.FixedPointNumber64;

/**
 * Default computation engine based on {@link FixedPointNumber64}.
 * 
 * 
 * @author Pascal Ollive
 * @version 1.1.0
 * @since Jun 11, 2018
 */
public class DefaultMandelbrotEngine extends MandelbrotEngine {

	private final long posX;
	private final long posY;

	public DefaultMandelbrotEngine(long posX, long posY, int levelMax) {
		super(posX, posY, levelMax);
		this.posX = posX;
		this.posY = posY;
	}

	@Override
	public float getIterationFromPoint(double shiftX, double shiftY) {
		final long x0 = FixedPointNumber64.sum(posX, shiftX);
		final long y0 = FixedPointNumber64.sum(posY, shiftY);

		long x = x0;
		long y = y0;

		long xe2 = FixedPointNumber64.square(posX, shiftX);
		long ye2 = FixedPointNumber64.square(posY, shiftY);

		int iteration = 0;

		while ((iteration < levelMax) && (!FixedPointNumber64.isSumOutOfRange(xe2, ye2))) {
			y = FixedPointNumber64.sum(FixedPointNumber64.twicedMul(x, y), y0);
			x = FixedPointNumber64.sum(FixedPointNumber64.unsafeDiff(xe2, ye2), x0);

			xe2 = FixedPointNumber64.square(x);
			ye2 = FixedPointNumber64.square(y);

			iteration = iteration + 1;
		}

		if (iteration == levelMax) {
			return levelMax;
		}
		return iteration + getFractionalIterationFromOrbitRadius(FixedPointNumber64.hypot(x, y));
	}

	@Override
	public boolean isResultEqualWith(double d1, double d2) {
		long x0 = FixedPointNumber64.sum(posX, d1);
		long xe2 = FixedPointNumber64.square(posX, d1);

		if ((x0 == FixedPointNumber64.sum(posX, d2)) && (xe2 == FixedPointNumber64.square(posX, d2))) {
			return true;
		}
		return false;
	}
}
