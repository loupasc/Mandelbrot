package net.fraktales.controller;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import net.fraktales.data.GeneratorParameters;
import net.fraktales.event.GeneratorListener;
import net.fraktales.tool.ColorConverter;
import net.fraktales.tool.ImageUtils;
import net.fraktales.tool.ProgressLog;

public class MandelbrotGenerator {
	private static final int DYNAMIC_LEVEL_FINDER_MAX_COUNT = 4;
	private static final double FAST_ENGINE_THRESHOLD = Math.scalb(1.0, -47);
	private final GeneratorParameters param;

	public MandelbrotGenerator(GeneratorParameters param) {
		this.param = param;
	}

	private static MandelbrotEngine createEngine(boolean isDefaultEngine, long posX, long posY, double step, int levelMax) {

		if (isDefaultEngine) {
			if (step > FAST_ENGINE_THRESHOLD) {
				return new FastMandelbrotEngine(posX, posY, levelMax);
			}
			return new DefaultMandelbrotEngine(posX, posY, levelMax);
		}

		return new FinestMandelbrotEngine(posX, posY, levelMax);
	}

	public float[] createBitmap() {
		if (param.isParamEnabled("aaEnabled")) {
			return new float[ImageUtils.getAntiAliasingEdgeSize(param.getIntegerValue("width"))
			                 * ImageUtils.getAntiAliasingEdgeSize(param.getIntegerValue("height"))];
		}
		return new float[param.getIntegerValue("width") * param.getIntegerValue("height")];
	}

	public int draw(float[] bitmap) {
		int width;
		int height;
		double step;
		if (param.isParamEnabled("aaEnabled")) {
			width = ImageUtils.getAntiAliasingEdgeSize(param.getIntegerValue("width"));
			height = ImageUtils.getAntiAliasingEdgeSize(param.getIntegerValue("height"));
			step = param.getFloatValue("step") / 2;
		}
		else {
			width = param.getIntegerValue("width");
			height = param.getIntegerValue("height");
			step = param.getFloatValue("step");
		}

		int numberOfCycles = DYNAMIC_LEVEL_FINDER_MAX_COUNT;
		int levelMax = 0;
		float iteration = 0; 
		float iterationMin = 0;
		int widthX2 = width * 2;
		int halfWidth = width / 2;
		int halfHeight = height / 2;

		MandelbrotEngine engine;
		do {
			numberOfCycles = numberOfCycles - 1;
			levelMax = levelMax + param.level.count;
			iteration = levelMax;
			iterationMin = levelMax;
			//
			// Compute only even lines
			//
			engine = createEngine(param.isParamEnabled("defaultEngine"), param.getFixedPointValue("x"), param.getFixedPointValue("y"), param.getFloatValue("step"), levelMax);
			for (int j = 0; j < halfHeight; j++) {
				int offset = widthX2 * j;
				double stepY = step * (2 * j - halfHeight);
				// force value to be different to the next step
				double previousStepX = step * (0 + halfWidth);
				for (int i = 0; i < width; i++) {
					double stepX = step * (i - halfWidth);
					if (!engine.isResultEqualWith(previousStepX, stepX)) {
						iteration = engine.getIterationFromPoint(stepX, stepY);

						if (iterationMin > iteration) {
							iterationMin = iteration;
						}
					}

					bitmap[offset] = iteration;
					previousStepX = stepX;
					offset = offset + 1;
				}
			}
		} while ((iterationMin >= levelMax) && (numberOfCycles > 0));

		if (iterationMin == 0) {
			//
			// Compute missing lines
			//
			for (int j = 0; j < halfHeight; j++) {
				int offset = widthX2 * j + width;
				double stepY = step * (2 * j + 1 - halfHeight);
				for (int i = 0; i < width; i++) {
					double stepX = step * (i - halfWidth);
					bitmap[offset] = engine.getIterationFromPoint(stepX, stepY);
					offset = offset + 1;
				}
			}
		}
		else {
			int oldLevelMax = levelMax;
			levelMax = (int) iterationMin + param.level.count;
			engine = createEngine(param.isParamEnabled("defaultEngine"), param.getFixedPointValue("x"), param.getFixedPointValue("y"), param.getFloatValue("step"), levelMax);
			for (int j = 0; j < height; j++) {
				int offset = width * j;
				double stepY = step * (j - halfHeight);
				// force value to be different to the next step
				double previousStepX = step * (0 + halfWidth);
				for (int i = 0; i < width; i++) {
					if ((bitmap[offset] == oldLevelMax) || ((j & 0x1) != 0)) {
						double stepX = step * (i - halfWidth);
						if (!engine.isResultEqualWith(previousStepX, stepX)) {
							iteration = engine.getIterationFromPoint(stepX, stepY);
						}
						previousStepX = stepX;
					}
					else {
						iteration = bitmap[offset];
					}
					bitmap[offset] = iteration - (int) iterationMin;
					offset = offset + 1;
				}
			}
		}

		int numberOfIterations = levelMax + 1;
		return numberOfIterations;
	}

	private static int[] colorizePixelBlocFromIteration(float[] bloc) {
		int[] rgbPixelBloc = new int[bloc.length];
		for (int i = 0; i < bloc.length; i++) {
			rgbPixelBloc[i] = ColorConverter.getRGBFromLevel(bloc[i]);
		}
		return rgbPixelBloc;
	}

	private static int clamp(int intensity) {
		if (intensity > 510) {
			return 510;
		}
		if (intensity < 0) {
			return 0;
		}
		return intensity;
	}

	public void writeRGB(String filename, GeneratorListener eventListener, int numberOfIterations) throws IOException {
		BufferedImage image = new BufferedImage(8 * param.getIntegerValue("width"),
				8 * param.getIntegerValue("height"),
				BufferedImage.TYPE_INT_RGB);
		int[] output = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();

		int fullWidth = ImageUtils.getAntiAliasingEdgeSize(image.getWidth());
		int fullHeight = ImageUtils.getAntiAliasingEdgeSize(image.getHeight());
		//
		// Anti-aliasing parameter is ignored and anti-aliasing is forced.
		// step = step div 16
		//
		double step = param.getFloatValue("step") / 16;
		int levelMax = numberOfIterations - 1;
		int levelMin = levelMax - param.level.count;

		int halfWidth = fullWidth / 2;
		int halfHeight = fullHeight / 2;
		float iteration = 0;

		//
		// Engine forced to best quality
		//
		FinestMandelbrotEngine engine = new FinestMandelbrotEngine(param.getFixedPointValue("x"), param.getFixedPointValue("y"), levelMax);

		//
		// Initialization of the renderer window.
		//
		float[] fullFractalSlice = new float[6 * fullWidth];
		for (int j = 0; j < 6; j++) {
			int offset = j * fullWidth;
			double stepY = step * (j - halfHeight);
			for (int i = 0; i < fullWidth; i++) {
				iteration = engine.getIterationFromPoint(step * (i - halfWidth), stepY);

				if (iteration < levelMin) {
					fullFractalSlice[offset] = 0;
				}
				else {
					fullFractalSlice[offset] = iteration - levelMin;
				}
				offset = offset + 1;
			}
		}

		float[] bloc6x6 = ImageUtils.create6x6PixelBloc();

		ProgressLog drawingProgress = new ProgressLog(image.getHeight());
		drawingProgress.initialize();
		for (int j = 0; j < image.getHeight(); j++) {

			String currentStatus = drawingProgress.getPercentageFrom(j);
			if (currentStatus != null) {
				StringBuilder sb = new StringBuilder("Writing ");
				sb.append(filename);
				sb.append(' ');
				sb.append(currentStatus);
				sb.append(" ...");
				eventListener.processingProgressUpdated(sb.toString());
			}

			int jMod2 = j & 1;
			int offset = 2 * fullWidth + 2;
			for (int i = 0; i < image.getWidth(); i++) {
				ImageUtils.fillInscribedCircleIn6x6PixelBloc(fullWidth, offset, fullFractalSlice, bloc6x6);
				int[] rgbPixelBloc = colorizePixelBlocFromIteration(bloc6x6);

				int red = clamp(ImageUtils.convolve(rgbPixelBloc, 0x7FC0000));
				int green = clamp(ImageUtils.convolve(rgbPixelBloc, 0x3FE00));
				int blue = clamp(ImageUtils.convolve(rgbPixelBloc, 0x1FF));

				int checksPattern = (i & 1) ^ jMod2;
				output[image.getWidth() * j + i] = ColorConverter.get24BitColor(red, green, blue, checksPattern);
				offset = offset + 2;
			}
			//
			// Two lines slide
			//
			System.arraycopy(fullFractalSlice, 2 * fullWidth, fullFractalSlice, 0, 4 * fullWidth);
			//
			// Next two lines
			//
			offset = 4 * fullWidth;
			for (int dj = 7; dj < 9; dj++) {
				double stepY = step * ((2 * j) + dj - halfHeight);
				for (int i = 0; i < fullWidth; i++) {
					iteration = engine.getIterationFromPoint(step * (i - halfWidth), stepY);

					if (iteration < levelMin) {
						fullFractalSlice[offset] = 0;
					}
					else {
						fullFractalSlice[offset] = iteration - levelMin;
					}
					offset = offset + 1;
				}
			}
		}

		ImageIO.write(image, "bmp", new File(filename));
	}
}
