package net.fraktales.controller;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

import net.fraktales.data.GeneratorParameters;
import net.fraktales.data.LevelSetting;
import net.fraktales.event.DefaultGeneratorListener;
import net.fraktales.event.GeneratorListener;


public class FractalManager implements Runnable {
	private static final String FILE_NAME = "mandelBig.bmp";
	private final GeneratorParameters param;
	private final ExecutorService generatorTask;
	private GeneratorListener eventListener;

	public FractalManager() {
		param = new GeneratorParameters();
		generatorTask = Executors.newSingleThreadExecutor(new MandelbrotThreadFactory());
		eventListener = new DefaultGeneratorListener();
	}

	public void setEventListener(GeneratorListener eventListener) {
		if (eventListener == null) throw new NullPointerException("null eventListener is forbidden");
		this.eventListener = eventListener;
	}

	public boolean isParamEnabled(String parameter) {
		return param.isParamEnabled(parameter);
	}

	public void setParam(String parameter, boolean value) {
		param.setParam(parameter, value);
	}

	public int getIntegerValue(String parameter) {
		return param.getIntegerValue(parameter);
	}

	public void setParam(String parameter, int value) {
		param.setParam(parameter, value);
	}

	public double getFloatValue(String parameter) {
		return param.getFloatValue(parameter);
	}

	public void setParam(String parameter, double value) {
		param.setParam(parameter, value);
	}

	public long getFixedPointValue(String parameter) {
		return param.getFixedPointValue(parameter);
	}

	public void setParam(String parameter, long value) {
		param.setParam(parameter, value);
	}

	public LevelSetting getLevelSetting() {
		return param.level;
	}

	public void setLevelSetting(LevelSetting level) {
		param.level = level;
	}

	public void execute() {
		eventListener.processingProgressUpdated("Start processing...");
		generatorTask.execute(this);
	}

	public void dispose() {
		generatorTask.shutdown();
	}

	@Override
	public void run() {
		GeneratorParameters p = param.clone();
		MandelbrotGenerator generator = new MandelbrotGenerator(p);

		if (p.isParamEnabled("bigFileEnabled")) {
			GeneratorParameters preview = new GeneratorParameters(p);
			MandelbrotGenerator miniGenerator = new MandelbrotGenerator(preview);
			float[] bitmap = miniGenerator.createBitmap();
			int numberOfIterations = miniGenerator.draw(bitmap);

			try {
				generator.writeRGB(FILE_NAME, eventListener, numberOfIterations);
				eventListener.fileExported(FILE_NAME);
			}
			catch (IOException e) {
				eventListener.processingError("Cannot generate " + FILE_NAME + ", " + e.getMessage());
			}
		}
		else {
			float[] bitmap = generator.createBitmap();
			int numberOfIterations = generator.draw(bitmap);
			eventListener.processingCompleted(p, numberOfIterations, bitmap);
		}
	}

	private static final class MandelbrotThreadFactory implements ThreadFactory {
		private MandelbrotThreadFactory() {
		}
		@Override
		public Thread newThread(Runnable r) {
			Thread t = new Thread(r, "MandelbrotEngine");
			t.setPriority(Thread.MAX_PRIORITY);
			return t;
		}
	}
}
