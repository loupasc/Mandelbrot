package net.fraktales.event;

import net.fraktales.data.GeneratorParameters;

public class DefaultGeneratorListener implements GeneratorListener {

	@Override
	public void processingProgressUpdated(String message) {
	}

	@Override
	public void processingCompleted(GeneratorParameters param, int numberOfIterations, float[] bitmap) {
	}

	@Override
	public void fileExported(String filename) {
	}

	@Override
	public void processingError(String cause) {
	}

	@Override
	public String toString() {
		return "DefaultGeneratorListener []";
	}
	
}
