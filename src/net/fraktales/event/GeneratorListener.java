package net.fraktales.event;

import net.fraktales.data.GeneratorParameters;

public interface GeneratorListener {
	public void processingProgressUpdated(String message);
	public void processingCompleted(GeneratorParameters param, int numberOfIterations, float[] bitmap);
	public void fileExported(String filename);
	public void processingError(String cause);
}
