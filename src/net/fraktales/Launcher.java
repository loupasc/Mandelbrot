package net.fraktales;

import javax.swing.SwingUtilities;

import net.fraktales.controller.FractalManager;
import net.fraktales.view.Mainscreen;

/**
 * Entry point of the Mandelbrot application.
 * 
 * @author Pascal Ollive
 * @version 1.1.2
 * @since Jun 11, 2018
 */
public final class Launcher {

	/** Application version. */
	private static final String VERSION = "1.1.2";

	/**
	 * Main class should not have instances.
	 */
	private Launcher() {
	}

	/**
	 * Entry point.
	 * 
	 * @param args application parameters
	 * @since Jun 11, 2018
	 */
	public static void main(String[] args) {
		Mainscreen view = new Mainscreen(VERSION, new FractalManager());

		SwingUtilities.invokeLater(view);
	}
}
